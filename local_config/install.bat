@echo off

set filename=local_uitesting_config.txt
set example=local_uitesting_config_example.txt
set folder=Run

if exist %UserProfile%\%folder%\%filename% (
	echo.
	echo Error. File already exists: %UserProfile%\%folder%\%filename%
	echo.
	pause
	start "title" explorer %UserProfile%\%folder%
) else (
	if exist %UserProfile%\%folder% (
		rem do nothing
	) else (
		md %UserProfile%\%folder%
	)
	echo.
	echo Copying file: %UserProfile%\%folder%\%filename%
	echo.
	copy %example% %UserProfile%\%folder%\%filename%
	echo.
	echo Open the config file and update the project path
	echo.
	pause
	start "title" explorer %UserProfile%\%folder%
)
exit
