$chrome="chromedriver.exe"
$ie="IEDriverServer.exe"
$opera="operadriver.exe"
$phantomjs="phantomjs.exe"
$edge="MicrosoftWebDriver.exe"
$firefox="firefox.exe"
$count=0

While 1
	If ProcessExists($chrome) Then
		ProcessClose($chrome)
		Sleep(125)
		$count=$count+1
	Else
		ExitLoop
	EndIf
WEnd

While 1
	If ProcessExists($ie) Then
		ProcessClose($ie)
		Sleep(125)
		$count=$count+1
	Else
		ExitLoop
	EndIf
WEnd

While 1
	If ProcessExists($opera) Then
		ProcessClose($opera)
		Sleep(125)
		$count=$count+1
	Else
		ExitLoop
	EndIf
WEnd

While 1
	If ProcessExists($phantomjs) Then
		ProcessClose($phantomjs)
		Sleep(125)
		$count=$count+1
	Else
		ExitLoop
	EndIf
WEnd

While 1
	If ProcessExists($edge) Then
		ProcessClose($edge)
		Sleep(125)
		$count=$count+1
	Else
		ExitLoop
	EndIf
WEnd

;While 1
;	If ProcessExists($firefox) Then
;		ProcessClose($firefox)
;		Sleep(125)
;		$count=$count+1
;	Else
;		ExitLoop
;	EndIf
;WEnd

;MsgBox(0,@ScriptName,$count&" processes closed")
ConsoleWrite($count&" processes closed"&@CRLF)

Exit
