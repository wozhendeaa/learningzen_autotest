package common;

/**
 * Created by miker on 5/27/2016.
 */
public class CreateText {

    public static String RandomLetters(int howMany, String capitalization) {
        String output = "";

        int A = 65;
        int Z = 90;
        int a = 97;
        int z = 122;

        if (capitalization.equals("lower")) {

            int randomLetter = RandomFromRange(a, z);
            char letter = (char) OmitLetter(randomLetter);
            output = ""+letter;

            for (int i = output.length(); i < howMany; i += 1) {
                randomLetter = RandomFromRange(a, z);
                letter = (char) OmitLetter(randomLetter);
                output = output+letter;
            }

        } else if (capitalization.equals("upper")) {

            int randomLetter = RandomFromRange(A, Z);
            char letter = (char) OmitLetter(randomLetter);
            output = ""+letter;

            for (int i = output.length(); i < howMany; i += 1) {
                randomLetter = RandomFromRange(A, Z);
                letter = (char) OmitLetter(randomLetter);
                output = output+letter;
            }

        } else if (capitalization.equals("first")) {

            int randomLetter = RandomFromRange(A, Z);
            char letter = (char) OmitLetter(randomLetter);
            output = ""+letter;

            for (int i = output.length(); i < howMany; i += 1) {
                randomLetter = RandomFromRange(a, z);
                letter = (char) OmitLetter(randomLetter);
                output = output+letter;
            }

        } else {
            System.out.println("capitalization must be lower, upper, or first");
        }

        return output;
    }

    private static int RandomFromRange(int min, int max) {
        int output = 0;

        int range = (max - min);
        double randomDouble = (Math.random() * range) + min;
        long randomLong = Math.round(randomDouble);
        output = Math.round(randomLong);

        return output;
    }

    // The letters "a" "i" "u" are omitted to reduce the chance of generating cuss words
    private static int OmitLetter(int input) {
        int output = input;

        int A = 65;
        int a = 97;
        int I = 73;
        int i = 105;
        int U = 85;
        int u = 117;

        if (output == a || output == A || output == i || output == I || output == u || output == U) {
            output += 1;
        }

        return output;
    }

    public static String RandomNumbers(int howMany) {
        String output = Integer.toString(RandomFromRange(0, 9));

        for (int i = output.length(); i < howMany; i += 1) {
            output = output+Integer.toString(RandomFromRange(0, 9));
        }

        return output;
    }
}
