package common;

/**
 * Created by miker on 5/27/2016.
 */

import java.io.*;

public class Log {

    public static String ReadLastStatus(String filePath, String fileName, String matchText) {
        String output = "";
        ArrayFromTextFile oldLogFile = new ArrayFromTextFile();
        String[] oldLogArray = oldLogFile.Read(filePath+"\\"+fileName);
        String matchTextLower = matchText.toLowerCase();
        int lineNumber = oldLogArray.length - 1;
        if (oldLogArray.length > 0) {
            for (int i = 0; i <= oldLogArray.length; i += 1) {
                if (oldLogArray[lineNumber].toLowerCase().contains(matchTextLower)) {
                    output = oldLogArray[lineNumber];
                    break;
                } else {
                    lineNumber -= 1;
                }
            }
        } else {
            // Do nothing
        }
        return output;
    }

    public static boolean Save(String filePath, String fileName, String message) {

        try {
            File logFile = new File(filePath + "\\" + fileName);
            long logFileSize = logFile.length();

            long maxFileSize = 1024 * 1024;

            File achiveFile = new File(filePath + "\\" + Chronos.CurrentDashDate() + "_" + fileName);
            if (logFileSize > maxFileSize) {
                if (achiveFile.exists()) {
                    System.out.println("Already exists: " + achiveFile.getAbsolutePath());
                } else {
                    logFile.renameTo(achiveFile);
                }
            }

            FileWriter logFileWriter = new FileWriter(logFile, true);
            BufferedWriter logFileBuffer = new BufferedWriter(logFileWriter);

            logFileBuffer.write(message);
            logFileBuffer.newLine();
            logFileBuffer.flush();
            logFileWriter.close();
            System.out.println(message);

            return true;
        } catch (FileNotFoundException fnfe){
            fnfe.printStackTrace();
            return false;
        }catch (IOException ioe) {
            ioe.printStackTrace();
            return false;
        }
    }
}
