package common;

/**
 * Created by miker on 5/27/2016.
 */
import java.util.Random;

public class Procedure {

    private static char chooseRandomCharacter() {
        Random randomInt = new Random();
        int random26 = randomInt.nextInt(26);
        // 0, 8, and 20 are incremented to omit letters a, i, and u - which reduces the possibility of generating cuss words
        if (random26 == 0){
            random26 = random26+1;
        }
        else if (random26 == 8){
            random26 = random26+1;
        }
        else if (random26 == 20){
            random26 = random26+1;
        }
        char character = 'a';
        character = (char) (character+random26);
        return character;
    }

    public static String makeRandomString(int length) {
        String output = "";
        for (int i = 0; i < length; i += 1)
            output = output+chooseRandomCharacter();
        return output;
    }

    public static String Fix(int input, int digits) {

        String output = Integer.toString(input);
        for (int i = digits; output.length() < i;) {
            output = "0"+output;
        }
        return output;
    }

    public static int chooseRandomNumber(int min, int max) {
        Random r = new Random();
        int result = r.nextInt(max+1-min)+min;
        return result;
    }

    public static String makeLine() {
        String line = "";
        for (int i = 0; i < 32; i += 1) {
            line = line+"-";
        }
        return "\n"+line;
    }

    public static String makeStars(String text) {
        String stars = "";
        for (int i = 0; i < text.length(); i += 1) {
            stars = stars+"*";
        }
        return stars;
    }

}
