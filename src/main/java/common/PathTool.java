package common;

/**
 * Created by miker on 5/27/2016.
 */
import java.io.File;

public class PathTool {

    public PathTool() {

    }

    public String Split(String path, int segments) throws PathSplitException {

        if (path.trim().isEmpty()) {
            throw new PathSplitException("Path is empty");
        }

        if (segments < 1) {
            throw new PathSplitException("Must specify more than zero segments");
        }

        String output = "";
        String[] parts = path.split("\\\\");
        int segmentCount = parts.length;

        if (segmentCount >= segments) {
            output = parts[0];
            for (int i = 1; i < segmentCount; i += 1) {
                if (i < segments) {
                    output += "\\"+parts[i];
                } else {
                    break;
                }
            }
        } else {
            throw new PathSplitException("Only "+segmentCount+" segments of "+segments+" in the path");
        }
        return output;
    }

    @SuppressWarnings("serial")
    class PathSplitException extends Exception {
        //Parameterless Constructor
        PathSplitException() {}

        //Constructor that accepts a message
        PathSplitException(String message) {
            super(message);
        }
    }

    public String Checker(String[] input) throws ExistanceCheckerException{

        int dependancyCount = input.length;

        if (dependancyCount < 1) {
            throw new ExistanceCheckerException("Must specify more than zero dependancies");
        }

        String output = "";
        ArraySplitter path = new ArraySplitter();
        String[] pathArray = path.Split("values", input);
        boolean ready = true;
        for (int i = 0; i < pathArray.length; i += 1) {
            File check = new File(pathArray[i]);
            if (check.exists()) {
                System.out.println("Verified: " + pathArray[i]);
            } else {
                System.out.println("MISSING: " + pathArray[i]);
                ready = false;
            }
        }
        if (ready == false) {
            throw new ExistanceCheckerException("One or more dependancies is missing");
        }
        return output;
    }

    @SuppressWarnings("serial")
    public class ExistanceCheckerException extends Exception {
        //Parameterless Constructor
        ExistanceCheckerException() {}

        //Constructor that accepts a message
        ExistanceCheckerException(String message) {
            super(message);
        }
    }
}
