package common;

/**
 * Created by miker on 5/27/2016.
 */

public class TextPair {

    private String delimiter;
    private int partsCount;
    private String key;
    private String value;

    TextPair() {

        delimiter = "=";

    }

    String readKey (String line) throws Exception {
        if (line.contains(delimiter)) {
            String[] parts = line.split("\\"+delimiter);
            partsCount = parts.length;
//			if (partsCount == 2) {
            key = parts[0].trim();
            return key;
//			} else {
//				throw new SplitCountExeption(partsCount);
//			}
        } else {
            throw new DelimiterExeption(line);
        }
    }

    String readValue (String line) throws Exception {
//		if (line.contains(delimiter)) {
        String[] parts = line.split("\\"+delimiter);
        partsCount = parts.length;
        if (partsCount == 2) {
            value = parts[1].trim();
            return value;
        } else {
            throw new SplitCountExeption(partsCount);
        }
//		} else {
//			throw new DelimiterExeption(line);
//		}
    }
}

class SplitCountExeption extends Exception {

    SplitCountExeption(int count) {

        super("Cannot determin value. Line split into "+count+" parts");
    }
}

class DelimiterExeption extends Exception {

    DelimiterExeption(String line) {

        super("Cannot find delimiter in: "+line);
    }
}
