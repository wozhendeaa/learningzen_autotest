package common;

/**
 * Created by miker on 5/27/2016.
 */
public class ArraySplitter {

    public ArraySplitter() {

    }

    public String Key(String keyName, String[] data) {

        if (keyName.isEmpty()) {
            String error = "The key is blank";
            return error;
        }

        if (data.length == 0) {
            String error = "The data array is empty";
            return error;
        }

        if (data.length % 2 != 0) {
            String error = "The data array contains an odd number of elements";
            return error;
        }

        ArraySplitter instance = new ArraySplitter();
        String[] keys = instance.Split("keys", data);
        String[] values = instance.Split("values", data);

        for (int i = 0; i < data.length / 2; i += 1) {

            for (int outer = 0; outer < keys.length; outer += 1) {
                for (int inner = 0; inner < keys.length; inner += 1) {
                    if (inner != outer) {
                        if (keys[inner].equals(keys[outer])) {
                            String error = "Duplicate key: "+keys[outer];
                            return error;
                        }
                    }
                }
            }

            if (keys[i].equals(keyName)) {
                String keyValuePair = values[i];
                return keyValuePair;
            }
        }

        String error = "Key not found: "+keyName;
        return error;
    }

    public String[] Split(String type, String[] data) {

        if (type.equals("keys")) {

            String[] keys = new String[data.length / 2];

            int k = 0;
            for (int i = 0; i < data.length / 2; i += 1) {
                keys[i] = data[k];
                k += 2;
            }

            return keys;

        } else if (type.equals("values")) {

            String[] values = new String[data.length / 2];

            int v = 1;
            for (int i = 0; i < data.length / 2; i += 1) {
                values[i] = data[v];
                v += 2;
            }

            return values;

        } else {
            String[] error = {"Unknown split type: "+type};
            return error;
        }
    }
}
