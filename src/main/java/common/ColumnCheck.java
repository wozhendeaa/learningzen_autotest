package common;

/**
 * Created by miker on 5/27/2016.
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ColumnCheck {


    public static String Sort(WebDriver driver, String columnLocator, String bindingStage, boolean reverse, boolean comboColumn) {

        try {

            String locator;
            WebElement element;

            int rowCount = Integer.parseInt(LocateElement.XpathCount(driver, columnLocator, bindingStage, 2));
            ArrayList<String> columnList = new ArrayList<String>();
            ArrayList<String> columnListSorted = new ArrayList<String>();
            String text;

            ArrayList<Float> columnFloatList = new ArrayList<Float>();
            ArrayList<Float> columnFloatListSorted = new ArrayList<Float>();
            float number;

            if (rowCount > 0) {
                for (int i = 1; i <= rowCount; i++) {

                    locator = columnLocator+"["+i+"]";
                    //LocateElement.Xpath(driver, locator, 2);
                    element = driver.findElement(By.xpath(locator));

                    // Replace newlines and then trim leading and trailing white-space
                    text = element.getText().replaceAll("[\\s]", " ").trim();

                    // Normalize all white-spaces to a single white-space
                    while (text.contains("[\\s]{2}")) {
                        text = text.replaceAll("[\\s]{2}", " ");
                    }

                    // If this is the first row by index, do some data-type evaluation before adding it to the arraylist
                    if (i == 1) {

                        // If the string contains a letter, use the string arraylist
                        if (text.matches(".*[a-zA-Z].*")){
/*							if (comboColumn == true) {
								//System.out.println("Parse: "+text);
								text = ParseCity(text);
							} else {
								//System.out.println("Just accept: "+text);
							}
*/							columnList.add(text);
                            columnListSorted.add(text);

                            // If the string contains no letters, but does contain a digit, use the float arraylist
                        } else if (text.matches(".*[0-9].*")){
                            number = Float.parseFloat(text.replaceAll("[^\\s\\.0-9]", ""));
                            columnFloatList.add(number);
                            columnFloatListSorted.add(number);

                            // Unable to determine the data type
                        } else if (text.isEmpty()){
                            throw new FirstRowEmptyException();

                            // Unable to determine the data type
                        } else {
                            throw new FirstRowDataTypeUnknownException(text);
                        }

                        // If this is not the first row by index, just populate the array
                    } else {

                        // Use the string arraylist if it is not empty
                        if (!columnList.isEmpty()) {

                            // Add the string to the string arraylist
/*							if (comboColumn == true) {
								//System.out.println("Parse: "+text);
								text = ParseCity(text);
							} else {
								//System.out.println("Just accept: "+text);
							}
*/							columnList.add(text);
                            columnListSorted.add(text);

                            // Use the float arraylist if it is not empty
                        } else if (!columnFloatList.isEmpty()) {

                            // Remove all the commas from the string and parse a float from it
                            number = Float.parseFloat(text.replaceAll(",", ""));

                            // Add the float the the float arraylist
                            columnFloatList.add(number);
                            columnFloatListSorted.add(number);

                            // Who knows what happened, but both arraylists are empty
                        } else {
                            throw new BothArrayListsAreEmptyException();
                        }
                    }
                }

                // See which direction the second array list should be sorted
                if (reverse == true) {

                    // If the string arraylist is not empty, sort it in reverse order
                    if (!columnListSorted.isEmpty()) {
                        Collections.sort(columnListSorted, Collections.reverseOrder());
                        System.out.println("Reverse string order");
                        System.out.println(columnListSorted);

                        // If the float arraylist is not empty, sort it in reverse order
                    } else if (!columnFloatList.isEmpty()) {
                        Collections.sort(columnFloatList, Collections.reverseOrder());
                        System.out.println("Reverse float order");
                        System.out.println(columnFloatList);

                        // This should never happen, but what if both arrays are empty?
                    } else {
                        throw new BothArrayListsAreEmptyException();
                    }

                    // See which direction the second array list should be sorted
                } else {

                    // If the string list is not empty, sort it
                    if (!columnListSorted.isEmpty()) {
                        Collections.sort(columnListSorted);
                        System.out.println("Maintain string order");
                        System.out.println(columnListSorted);

                        // If the float arraylist is not empty, sort it
                    } else if (!columnFloatList.isEmpty()) {
                        Collections.sort(columnFloatList);
                        System.out.println("Maintain float order");
                        System.out.println(columnFloatList);

                        // This should never happen, but what if both arrays are empty?
                    } else {
                        throw new BothArrayListsAreEmptyException();
                    }
                }
            }

            // If the string arraylist is not empty, compare it with the sorted string arraylist
            if (!columnListSorted.isEmpty()) {
                if (columnList.size() > 1 && columnList.size() == columnListSorted.size()) {
                    for (int i = 0; i < columnList.size(); i++) {
                        if (columnList.get(i).equals(columnListSorted.get(i))) {
                            System.out.println(i+1+": "+columnList.get(i)+" = "+columnListSorted.get(i));

                        } else {
                            throw new RowStringSortMismatchException(i+1, columnList.get(i), columnListSorted.get(i));
                        }
                    }
                    System.out.println("Column sorting is in agreement");
                }
                return Integer.toString(columnList.size());

                // If the float arraylist is not empty, compare it with the sorted float arraylist
            } else if (!columnFloatList.isEmpty()) {
                if (columnFloatList.size() > 1 && columnFloatList.size() == columnFloatListSorted.size()) {
                    for (int i = 0; i < columnFloatList.size(); i++) {
                        if (columnFloatList.get(i).equals(columnFloatListSorted.get(i))) {
                            System.out.println(i+1+": "+columnFloatList.get(i)+" = "+columnFloatListSorted.get(i));

                        } else {
                            throw new RowNumberSortMismatchException(i+1, columnFloatList.get(i), columnFloatListSorted.get(i));
                        }
                    }
                    System.out.println("Column sorting is in agreement");
                }
                return Integer.toString(columnList.size());

                // This should never happen, but what if both arrays are empty?
            } else {
                throw new BothArrayListsAreEmptyException();
            }

        } catch (Exception all) {

            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;

        }
    }

/*	public static int FloatSort(WebDriver driver, String columnLocator, boolean reverse) throws Exception {

		String locator;
		WebElement element;

		int rowCount = LocateElement.XpathCount(driver, columnLocator, 2);
		ArrayList<Float> columnFloatList = new ArrayList<Float>();
		ArrayList<Float> columnFloatListSorted = new ArrayList<Float>();
		float number;
		if (rowCount > 0) {
			for (int i = 1; i <= rowCount; i++) {
				locator = columnLocator+"["+i+"]";
				//LocateElement.Xpath(driver, locator, 2);
				element = driver.findElement(By.xpath(locator));
				number = Float.parseFloat(element.getText().trim().replaceAll(",", ""));
				columnFloatList.add(number);
				columnFloatListSorted.add(number);
			}
			if (reverse == true) {
				Collections.sort(columnFloatListSorted, Collections.reverseOrder());
			} else {
				Collections.sort(columnFloatListSorted);
			}
		}
		if (columnFloatList.size() > 1 && columnFloatList.size() == columnFloatListSorted.size()) {
			for (int i = 0; i < columnFloatList.size(); i++) {
				if (columnFloatList.get(i).equals(columnFloatListSorted.get(i))) {
					System.out.println(i+1+": "+columnFloatList.get(i)+" = "+columnFloatListSorted.get(i));
				} else {
					throw new RowNumberSortMismatchException(i+1, columnFloatList.get(i), columnFloatListSorted.get(i));
				}
			}
			System.out.println("Column sorting is in agreement");
		}
		return columnFloatList.size();
	}*/

    private static String ParseCity(String input) {
        String output = "";
        Pattern p = Pattern.compile(", [a-zA-Z]{2}$");
        Matcher m = p.matcher(input);
        if (m.find()) {
            String end = m.group();
            String[] start = input.split(end);
            output = start[0];
        } else {
            output = input;
        }
        return output;
    }
}

class RowStringSortMismatchException extends Exception {

    RowStringSortMismatchException(int row, String column, String columnSorted) {

        super("Row "+row+" is "+column+" but should be "+columnSorted);
    }
}

class RowNumberSortMismatchException extends Exception {

    RowNumberSortMismatchException(int row, float column, float columnSorted) {

        super("Row "+row+" is "+column+" but should be "+columnSorted);
    }
}

class FirstRowEmptyException extends Exception {

    FirstRowEmptyException() {

        super("Unable to determin the data-type of the first row because it is empty");
    }
}

class FirstRowDataTypeUnknownException extends Exception {

    FirstRowDataTypeUnknownException(String text) {

        super("Unable to determin the data-type of the first row: "+text);
    }
}

class BothArrayListsAreEmptyException extends Exception {

    BothArrayListsAreEmptyException() {

        super("Cannot determin which ArrayList to use because they are both empty");
    }
}
