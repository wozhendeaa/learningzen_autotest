package common;

/**
 * Created by miker on 5/27/2016.
 */
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class ArrayFromTextFile {

    public ArrayFromTextFile() {

    }

    public String[] Read(String fileName) {

        try {

            // Create a buffered input stream reader for the text file
            FileInputStream stream = new FileInputStream(fileName);
            InputStreamReader reader = new InputStreamReader(stream);
            BufferedReader buffer = new BufferedReader(reader);

            // Create a second buffered input stream reader for the text file
            FileInputStream streamLength = new FileInputStream(fileName);
            InputStreamReader readerLength = new InputStreamReader(streamLength);
            BufferedReader bufferLength = new BufferedReader(readerLength);

            // Use the second stream reader to get the number of lines that the text file contains
            int lineCount = (int) bufferLength.lines().count();

            // Close the second stream reader
            bufferLength.close();

            // Use the first stream reader to read each line of the text file
            String line = "";
            String[] fileLines = new String[lineCount];
            int fillCount = 0;
            for (int i = 0; i < lineCount; i += 1) {

                line = buffer.readLine().trim();

                // Skip any lines containing only white space characters
                if (line.isEmpty()) {
                    // Do nothing
                } else {
                    // Count lines that are not empty
                    fileLines[fillCount] = line;
                    fillCount += 1;
                }
            }

            // Close the first buffered stream reader
            buffer.close();

            // Create a new array that is the same size as the number of filled lines
            String[] fileLinesTrimed = new String[fillCount];

            // Return the array only if it contains something
            if (fillCount > 0) {

                // Fill the new array with data
                for (int i = 0; i < fillCount ; i += 1) {
                    fileLinesTrimed[i] = fileLines[i];
                }

                // Return the array with the content of the text file
                return fileLinesTrimed;

            } else {

                String[] error = {"File is empty: "+fileName};
                return error;
            }

        } catch (Exception e) {
            e.printStackTrace();
            String[] error = {};
            return error;
        }
    }
}
