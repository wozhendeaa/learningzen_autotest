package common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.*;
import java.util.Arrays;

/**
 * Created by miker on 6/28/2016.
 */
public class WebControl {

    public static WebDriver StartDriver(String currentDriver, String[] pathArray) {

        ArraySplitter path = new ArraySplitter();

        String localConfigPath = path.Key(PackageManifest.localConfigPath, pathArray);
        String projectPath = path.Key(PackageManifest.project, pathArray);
        String edgePath = path.Key(PackageManifest.edge, pathArray);
        String chromePath = path.Key(PackageManifest.chrome, pathArray);
        String iexplorePath = path.Key(PackageManifest.iexplore, pathArray);
        String operaPath = path.Key(PackageManifest.opera, pathArray);
        String marionettePath = path.Key(PackageManifest.marionette, pathArray);
        String phantomPath = path.Key(PackageManifest.phantom, pathArray);

        String configFilePath = localConfigPath+"\\local_uitesting_config.txt";
        ArrayFromTextFile configFile = new ArrayFromTextFile(); // This is a custom class that reads a text file into an array.
        String[] configArray = configFile.Read(configFilePath);
        ArraySplitter config = new ArraySplitter(); // This is a custom class that locates a specific key in a structured array.

        String driverNameEdge = config.Key("Driver_Name_Edge", configArray);
        String driverNameChrome = config.Key("Driver_Name_Chrome", configArray);
        String driverNameIexplore = config.Key("Driver_Name_Internet_Explorer", configArray);
        String driverNameOpera = config.Key("Driver_Name_Opera", configArray);
        String driverNameMarionette = config.Key("Driver_Name_Marionette", configArray);
        String driverNameFirefox = config.Key("Driver_Name_Firefox", configArray);
        String driverNameHtml = config.Key("Driver_Name_Html", configArray);
        String driverNamePhantom = config.Key("Driver_Name_PhantomJS", configArray);

        if (currentDriver.isEmpty()) {

            System.out.println("There was no driver specified.");
            WebDriver driver = null;
            return driver;

        } else if (currentDriver.equals(driverNameEdge)) {

            DesiredCapabilities edgeCaps = new DesiredCapabilities();
            System.setProperty("webdriver.edge.driver", edgePath);
            edgeCaps.setBrowserName("edge");
            edgeCaps.setJavascriptEnabled(true);
            edgeCaps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            edgeCaps.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
            WebDriver driver = new EdgeDriver(edgeCaps);
            return driver;

        } else if (currentDriver.equals(driverNameChrome)) {

            DesiredCapabilities chromeCaps = new DesiredCapabilities();
            System.setProperty("webdriver.chrome.driver", chromePath);
            chromeCaps.setBrowserName("chrome");
            chromeCaps.setJavascriptEnabled(true);
            chromeCaps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            chromeCaps.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
            WebDriver driver = new ChromeDriver(chromeCaps);
            return driver;

        } else if (currentDriver.equals(driverNameIexplore)) {
        /*
         * InternetExplorerDriver configuration to avoid NoSuchWindowException.
         * In the "Advanced Settings" in "Internet Options", uncheck "Enable Enhanced Protected Mode*"
         * In "Security" in "Internet Options", check-mark "Enable Protected Mode" for all 4 zones.
         * In the "sites" list within the "Trusted sites" zone, uncheck "Require server verification (https)"
         * Add a wild-card exemption to the list for the domain, like "*.google.com" for example.
         */
            DesiredCapabilities iexploreCaps = new DesiredCapabilities();
            System.setProperty("webdriver.ie.driver", iexplorePath);
            iexploreCaps.setBrowserName("internet explorer");
            iexploreCaps.setJavascriptEnabled(true);
            iexploreCaps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true); // It may be necessary to install the SSL certificate in the Trusted Root Certification Authorities group
            iexploreCaps.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
//            iexploreCaps.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
//            iexploreCaps.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, false);
            WebDriver driver = new InternetExplorerDriver(iexploreCaps);
            return driver;

        } else if (currentDriver.equals(driverNameOpera)) {

//            https://github.com/operasoftware/operachromiumdriver/releases
            DesiredCapabilities operaCaps = new DesiredCapabilities();
            System.setProperty("webdriver.opera.driver", operaPath);
            operaCaps.setBrowserName("opera");
            operaCaps.setJavascriptEnabled(true);
            operaCaps.setCapability("opera.profile", new String(System.getProperty("user.home")+"\\AppData\\Roaming\\Opera Software\\Opera Stable"));
            operaCaps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true); // It may be necessary to install the SSL certificate in the Trusted Root Certification Authorities group
            operaCaps.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
            WebDriver driver = new OperaDriver(operaCaps);
            return driver;

        } else if (currentDriver.equals(driverNameMarionette)) {

            DesiredCapabilities marionetteCaps = new DesiredCapabilities();
            System.setProperty("webdriver.gecko.driver", marionettePath);
            marionetteCaps.setBrowserName("gecko");
            marionetteCaps.setCapability("marionette", true);
            marionetteCaps.setJavascriptEnabled(true);
            marionetteCaps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true); // It may be necessary to install the SSL certificate in the Trusted Root Certification Authorities group
            marionetteCaps.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
            WebDriver driver = new MarionetteDriver(marionetteCaps);
            return driver;

        } else if (currentDriver.equals(driverNameFirefox)) {

            DesiredCapabilities firefoxCaps = new DesiredCapabilities();
            firefoxCaps.setBrowserName("firefox");
            firefoxCaps.setJavascriptEnabled(true);
            firefoxCaps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            firefoxCaps.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
            WebDriver driver = new FirefoxDriver(firefoxCaps);
            return driver;

        } else if (currentDriver.equals(driverNameHtml)) {

            DesiredCapabilities htmlUnitCaps = new DesiredCapabilities();
            htmlUnitCaps.setBrowserName("htmlunit");
            htmlUnitCaps.setJavascriptEnabled(true);
            htmlUnitCaps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            WebDriver driver = new HtmlUnitDriver(htmlUnitCaps);
            return driver;

        } else if (currentDriver.equals(driverNamePhantom)) {

            DesiredCapabilities phantomCaps = new DesiredCapabilities();
            phantomCaps.setBrowserName("phantomjs");
            phantomCaps.setJavascriptEnabled(true);
            phantomCaps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, phantomPath);
            phantomCaps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--ignore-ssl-errors=true", "--ssl-protocol=any", "--web-security=false",});
            WebDriver driver = new PhantomJSDriver(phantomCaps);
            return driver;

        } else {

            System.out.println("Unknown driver: "+currentDriver);
            WebDriver driver = null;
            return driver;
        }
    }

    public static void SeleniumServerTerminator(String[] args, String[] pathArray) {

        ArraySplitter path = new ArraySplitter();

        String projectPath = path.Key(PackageManifest.project, pathArray);
        String autoitPath = path.Key(PackageManifest.autoit, pathArray);

        String terminatorPath = projectPath+"\\autoit_binary\\SeleniumServerStop.au3";
        File scriptFile = new File(terminatorPath);
        if (scriptFile.exists()) {
            try {
                Process process = new ProcessBuilder(autoitPath, terminatorPath).start();
                InputStream is = process.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line;
                System.out.printf("All sequences finished, ", Arrays.toString(args));
                while ((line = br.readLine()) != null) {
                    System.out.println(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Cannot find: "+terminatorPath);
        }
    }
}
