package common;

/**
 * Created by miker on 5/27/2016.
 */
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class Chronos {

    // long have to have L notation at the end
    static long dayMilliseconds = 24*60*60*1000L;

    static long minuteMilliseconds = 60*1000L;

    private static String Fix(int input, int digits) {

        String output = Integer.toString(input);
        for (int i = digits; output.length() < i;) {
            output = "0"+output;
        }
        return output;
    }

    public static String CurrentDashDate() {
        Calendar today = new GregorianCalendar();

        String year = Chronos.Fix(today.get(Calendar.YEAR), 2);
        String month = Chronos.Fix((today.get(Calendar.MONTH))+1, 2);
        String dayOfMonth = Chronos.Fix(today.get(Calendar.DAY_OF_MONTH), 2);

        return year+"-"+month+"-"+dayOfMonth;
    }

    public static String CurrentSlashDate() {
        Calendar today = new GregorianCalendar();

        String year = Chronos.Fix(today.get(Calendar.YEAR), 2);
        String month = Chronos.Fix((today.get(Calendar.MONTH))+1, 2);
        String dayOfMonth = Chronos.Fix(today.get(Calendar.DAY_OF_MONTH), 2);

        return month+"/"+dayOfMonth+"/"+year;
    }

    public static String CurrentTime() {
        Calendar today = new GregorianCalendar();

        String hourOfDay = Chronos.Fix(today.get(Calendar.HOUR_OF_DAY), 2);
        String minute = Chronos.Fix(today.get(Calendar.MINUTE), 2);
        String second = Chronos.Fix(today.get(Calendar.SECOND), 2);
        String milli = Chronos.Fix(today.get(Calendar.MILLISECOND), 3);
        TimeZone zone = today.getTimeZone();
        int offset = zone.getRawOffset();
        int savings = zone.getDSTSavings();
        String utc = Integer.toString((savings+offset)/1000/60/60);

        return hourOfDay+":"+minute+":"+second+"."+milli+" UTC"+utc;
    }

    public static String CurrentDayWithZero() {
        Calendar today = new GregorianCalendar();

        String dayOfMonth = Chronos.Fix(today.get(Calendar.DAY_OF_MONTH), 2);

        return dayOfMonth;
    }

    public static String CurrentMonthWithZero() {
        Calendar today = new GregorianCalendar();

        String month = Chronos.Fix((today.get(Calendar.MONTH))+1, 2);

        return month;
    }

    public static String CurrentYear() {
        Calendar today = new GregorianCalendar();

        String year = Chronos.Fix(today.get(Calendar.YEAR), 2);

        return year;
    }

    public static String PastMilitaryTime(long minutesAgo) {
        Date today = new Date();
        long goback = today.getTime() - (minutesAgo * minuteMilliseconds);
        Calendar past = new GregorianCalendar();
        past.setTimeInMillis(goback);
        String hourOfDay = Chronos.Fix(past.get(Calendar.HOUR_OF_DAY), 2);
        String minute = Chronos.Fix(past.get(Calendar.MINUTE), 2);

        return hourOfDay+""+minute+"";
    }

    public static String PastTime(long minutesAgo) {
        Date today = new Date();
        long goback = today.getTime() - (minutesAgo * minuteMilliseconds);
        Calendar past = new GregorianCalendar();
        past.setTimeInMillis(goback);
        String hour = Chronos.Fix(past.get(Calendar.HOUR), 2);
        String minute = Chronos.Fix(past.get(Calendar.MINUTE), 2);
//        String second = Chronos.Fix(past.get(Calendar.SECOND), 2);
//        String milli = Chronos.Fix(past.get(Calendar.MILLISECOND), 3);
//        TimeZone zone = past.getTimeZone();
//        int offset = zone.getRawOffset();
//        int savings = zone.getDSTSavings();
//        String utc = Integer.toString((savings+offset)/1000/60/60);

        return hour+":"+minute;
    }

    public static String PastMeridiem(long minutesAgo) {
        Date today = new Date();
        long goback = today.getTime() - (minutesAgo * minuteMilliseconds);
        Calendar past = new GregorianCalendar();
        past.setTimeInMillis(goback);
        String am_pm = Chronos.Fix(past.get(Calendar.AM_PM), 2);
        String meridiem = "";

        if (Integer.parseInt(am_pm) == 0) {
            meridiem = "am";
        }

        if (Integer.parseInt(am_pm) == 1) {
            meridiem = "pm";
        }

        return meridiem;
    }

    public static String PastDayWithZero(long daysAgo) {
        Date today = new Date();
        long goback = today.getTime() - (daysAgo * dayMilliseconds);
        Calendar past = new GregorianCalendar();
        past.setTimeInMillis(goback);
        String dayOfMonth = Chronos.Fix(past.get(Calendar.DAY_OF_MONTH), 2);
        if (past.get(Calendar.DAY_OF_MONTH) > 28) {
            dayOfMonth = Chronos.Fix(28, 2);
        }
        return dayOfMonth;
    }

    public static String PastMonthWithZero(long daysAgo) {
        Date today = new Date();
        long goback = today.getTime() - (daysAgo * dayMilliseconds);
        Calendar past = new GregorianCalendar();
        past.setTimeInMillis(goback);
        String month = Chronos.Fix(past.get(Calendar.MONTH), 2);
        return month;
    }

    public static String PastYear(long daysAgo) {
        Date today = new Date();
        long goback = today.getTime() - (daysAgo * dayMilliseconds);
        Calendar past = new GregorianCalendar();
        past.setTimeInMillis(goback);
        String year = Chronos.Fix(past.get(Calendar.YEAR), 2);
        return year;
    }

    // https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
    public static String PastDate(String format, long daysAgo) {
        Date today = new Date();
        long goBack = today.getTime() - (daysAgo * dayMilliseconds);
        SimpleDateFormat setFormat = new SimpleDateFormat(format);
        Calendar past = new GregorianCalendar();
        past.setTimeInMillis(goBack);
        String formatted = setFormat.format(past.getTime());
        return formatted;
    }
}
