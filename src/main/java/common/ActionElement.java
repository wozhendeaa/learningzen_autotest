package common;

/**
 * Created by miker on 5/27/2016.
 */
import java.util.regex.*;

import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ActionElement {

    public static String ClickIgnoreAlert (WebDriver driver, WebElement element, int timeout) {
        String output = "";
        while (true) {
            try {
                output = ActionElement.Click(driver, element, timeout);
                return output;
            } catch (UnhandledAlertException uae) {
                while (true) {
                    try {
                        Alert alert = driver.switchTo().alert();
                        alert.accept();
                        break;
                    } catch (NoAlertPresentException nap) {
                        System.out.println(nap);
                        return output;
                    }
                }
            }
        }
    }

    public static String Click (WebDriver driver, WebElement element, int timeout) {
        String output = "";
        System.out.println("Click");
        try {
            element.click();
            output = "success";
            return output;
        } catch (Exception e1) {
            for (int i = 1; i < timeout*8; i++) {
                System.out.println("Click");
                try {
                    Thread.sleep(125);
                    element.sendKeys("");
                    element.click();
                    output = "success";
                    return output;
                } catch (Exception e2) {
                    e1 = e2;
                }
            }
            System.out.println(e1);
            output = "failure";
            return output;
        }
    }

    public static String TypeIgnoreAlert (WebDriver driver, WebElement element, int timeout, String text, boolean tab, boolean log) {
        String output = "";
        while (true) {
            try {
                output = ActionElement.Type(driver, element, timeout, text, tab, log);
                return output;
            } catch (UnhandledAlertException uae) {
                while (true) {
                    try {
                        Alert alert = driver.switchTo().alert();
                        alert.accept();
                        break;
                    } catch (NoAlertPresentException nap) {
                        System.out.println(nap);
                        return output;
                    }
                }
            }
        }
    }

    public static String Type (WebDriver driver, WebElement element, int timeout, String text, boolean tab, boolean log) {
        String output = "";
        String whatItIs = "";
        String shouldBe = "";
        int count = 0;
        int step = 1;
        while (step > 0) {
            try {
                switch(step) {

                    case 1:

                        element.clear();
                        step += 1;

                    case 2:

                        element.sendKeys(text);
                        if (log == true) {
                            System.out.println("Type: "+text);
                        } else {
                            System.out.println("Type: "+Procedure.makeStars(text));
                        }
                        step += 1;

                    case 3:

                        whatItIs = element.getAttribute("value").trim().toLowerCase();
                        shouldBe = text.trim().toLowerCase();

                        if (whatItIs.equals(shouldBe)) {
                            step += 1;
                            output = "success";
                        } else {
                            System.out.println("The field is "+whatItIs+" but should be "+shouldBe);
                            step = 1; // Switch to case step
                            break; // Exit switch
                        }

                    case 4:

                        if (tab == true) {
                            element.sendKeys(Keys.TAB);
                        }
                        step = 0; // Exit while loop
                        break; // Exit switch
                }
//				System.out.println("i'm at step "+step);
            } catch (UnhandledAlertException uae) {
                try {
                    Alert alert = driver.switchTo().alert();
                    alert.accept();
                } catch (NoAlertPresentException nap) {
                    System.out.println(nap);
                }
            } catch (Exception all) {
                if (count < (timeout*8)) {
                    step = 1; // Switch to case step
                    try {
                        Thread.sleep(125);
                    } catch (InterruptedException ie) {
                        System.out.println(ie);
                    }
                } else {
                    step = 0; // Exit while loop
                    output = ""+all;
                    break;
                }
            }
        }
        return output;
    }

    public static String SelectOptionIgnoreAlert (WebDriver driver, WebElement element, String text, int timeout) {
        String output = "";
        while (true) {
            try {
                output = ActionElement.SelectOption(driver, element, text, timeout);
                return output;
            } catch (UnhandledAlertException uae) {
                while (true) {
                    try {
                        Alert alert = driver.switchTo().alert();
                        alert.accept();
                        break;
                    } catch (NoAlertPresentException nap) {
                        System.out.println(nap);
                        return output;
                    }
                }
            }
        }
    }

    public static String SelectOption (WebDriver driver, WebElement element, String text, int timeout) {
        String output = "";

        Select option = new Select(element);

        int count = 0;
        int step = 1;
        while (step > 0) {
            try {
                switch(step) {

                    case 1:

                        option.selectByVisibleText(text);
                        System.out.println("Select: "+text);
                        step += 1;

                    case 2:

                        output = "success";
                        step += 1;
                }
                break;
            } catch (UnhandledAlertException uae) {
                try {
                    Alert alert = driver.switchTo().alert();
                    alert.accept();
                } catch (NoAlertPresentException nap) {
                    System.out.println(nap);
                }
            } catch (Exception all) {
                if (count < (timeout*8)) {
                    step = 1; // Switch to case step
                    try {
                        Thread.sleep(125);
                    } catch (InterruptedException ie) {
                        System.out.println(ie);
                    }
                } else {
                    step = 0; // Exit while loop
                    output = ""+all;
                    break;
                }
            }
            count += 1;
        }
        return output;
    }

    public static String SelectIndexIgnoreAlert (WebDriver driver, WebElement element, int index, int timeout) {
        String output = "";
        while (true) {
            try {
                output = ActionElement.SelectIndex(driver, element, index, timeout);
                return output;
            } catch (UnhandledAlertException uae) {
                while (true) {
                    try {
                        Alert alert = driver.switchTo().alert();
                        alert.accept();
                        break;
                    } catch (NoAlertPresentException nap) {
                        System.out.println(nap);
                        return output;
                    }
                }
            }
        }
    }

    public static String SelectIndex (WebDriver driver, WebElement element, int index, int timeout) {
        String output = "";

        Select option = new Select(element);

        int count = 0;
        int step = 1;
        while (step > 0) {
            try {
                switch(step) {

                    case 1:

                        option.selectByIndex(index);
                        System.out.println("Select index: "+index);
                        step += 1;

                    case 2:

                        output = "success";
                        step += 1;

                }
                break;
            } catch (UnhandledAlertException uae) {
                try {
                    Alert alert = driver.switchTo().alert();
                    alert.accept();
                } catch (NoAlertPresentException nap) {
                    System.out.println(nap);
                }
            } catch (Exception all) {
                if (count < (timeout*8)) {
                    step = 1; // Switch to case step
                    try {
                        Thread.sleep(125);
                    } catch (InterruptedException ie) {
                        System.out.println(ie);
                    }
                } else {
                    step = 0; // Exit while loop
                    output = ""+all;
                    break;
                }
            }
            count += 1;
        }
        return output;
    }

    public static String SelectValueIgnoreAlert (WebDriver driver, WebElement element, String text, int timeout) {
        String output = "";
        while (true) {
            try {
                output = ActionElement.SelectValue(driver, element, text, timeout);
                return output;
            } catch (UnhandledAlertException uae) {
                while (true) {
                    try {
                        Alert alert = driver.switchTo().alert();
                        alert.accept();
                        break;
                    } catch (NoAlertPresentException nap) {
                        System.out.println(nap);
                        return output;
                    }
                }
            }
        }
    }

    public static String SelectValue (WebDriver driver, WebElement element, String text, int timeout) {
        String output = "";

        Select option = new Select(element);

        int count = 0;
        int step = 1;
        while (step > 0) {
            try {
                switch(step) {

                    case 1:

                        option.selectByValue(text);
                        System.out.println("Select value: "+text);
                        step += 1;

                    case 2:

                        output = "success";
                        step += 1;

                }
                break;
            } catch (UnhandledAlertException uae) {
                try {
                    Alert alert = driver.switchTo().alert();
                    alert.accept();
                } catch (NoAlertPresentException nap) {
                    System.out.println(nap);
                }
            } catch (Exception all) {
                if (count < (timeout*8)) {
                    step = 1; // Switch to case step
                    try {
                        Thread.sleep(125);
                    } catch (InterruptedException ie) {
                        System.out.println(ie);
                    }
                } else {
                    step = 0; // Exit while loop
                    output = ""+all;
                    break;
                }
            }
            count += 1;
        }
        return output;
    }

    public static String ReadIgnoreAlert (WebDriver driver, WebElement element, int timeout) {
        String output = "";
        while (true) {
            try {
                output = ActionElement.Read(driver, element, timeout);
                return output;
            } catch (UnhandledAlertException uae) {
                while (true) {
                    try {
                        Alert alert = driver.switchTo().alert();
                        alert.accept();
                        break;
                    } catch (NoAlertPresentException nap) {
                        System.out.println(nap);
                        return output;
                    }
                }
            }
        }
    }

    public static String Read (WebDriver driver, WebElement element, int timeout) {
        String capture;
        try {
            capture = element.getText().trim();
            System.out.println("Read "+capture);
            return capture;
        } catch (Exception e1) {
            for (int i = 1; i < timeout*8; i++) {
                try {
                    Thread.sleep(125);
                    capture = element.getText().trim();
                    System.out.println("Read "+capture);
                    return capture;
                } catch (Exception e2) {
                    e1 = e2;
                }
            }
            System.out.println(e1);
            return "";
        }
    }

    public static String ReadOptionIgnoreAlert (WebDriver driver, WebElement element, int timeout) {
        String output = "";
        while (true) {
            try {
                output = ActionElement.ReadOption(driver, element, timeout);
                return output;
            } catch (UnhandledAlertException uae) {
                while (true) {
                    try {
                        Alert alert = driver.switchTo().alert();
                        alert.accept();
                        break;
                    } catch (NoAlertPresentException nap) {
                        System.out.println(nap);
                        return output;
                    }
                }
            }
        }
    }

    public static String ReadOption (WebDriver driver, WebElement element, int timeout) {
        String capture;
        Select option = new Select(element);
        try {
            capture = option.getFirstSelectedOption().getAttribute("value").trim();
            System.out.println("Read "+capture);
            return capture;
        } catch (Exception e1) {
            for (int i = 1; i < timeout*8; i++) {
                try {
                    Thread.sleep(125);
                    capture = option.getFirstSelectedOption().getAttribute("value").trim();
                    System.out.println("Read "+capture);
                    return capture;
                } catch (Exception e2) {
                    e1 = e2;
                }
            }
            System.out.println(e1);
            return "";
        }
    }

    public static String ReadHeadingIntegerIgnoreAlert (WebDriver driver, WebElement element, int timeout) {
        String output = "";
        while (true) {
            try {
                output = ActionElement.ReadHeadingInteger(driver, element, timeout);
                return output;
            } catch (UnhandledAlertException uae) {
                while (true) {
                    try {
                        Alert alert = driver.switchTo().alert();
                        alert.accept();
                        break;
                    } catch (NoAlertPresentException nap) {
                        System.out.println(nap);
                        return output;
                    }
                }
            }
        }
    }

    public static String ReadHeadingInteger(WebDriver driver, WebElement element, int timeout) {

        String capture;
        String output = "0";
        try {
            capture = element.getText();
            output = ParseHeadingDigits(capture);
            return output;
        } catch (Exception e1) {
            for (int i = 1; i < timeout*8; i++) {
                try {
                    Thread.sleep(125);
                    capture = element.getText();
                    output = ParseHeadingDigits(capture);
                    return output;
                } catch (Exception e2) {
                    e1 = e2;
                }
            }
            System.out.println(e1);
            return "0";
        } finally {
            System.out.println("Parse digits: "+output);
        }
    }

    private static String ParseHeadingDigits(String input) {
        String output = "0";
        String onePage = " items found, displaying all items.";
        String multiplePages = " items found, displaying 1 to ";
        String oneRow = " item found";
        if (input.contains(onePage)) {
            System.out.println("One page");
            Pattern readDigits = Pattern.compile("\\d+");
            Matcher makeMatch = readDigits.matcher(input);
            makeMatch.find();
            input = makeMatch.group();
            output = Integer.toString(Integer.parseInt(input));
        } else if (input.contains(multiplePages)) {
            System.out.println("Multiple pages");
            Pattern readDigits = Pattern.compile("\\d+");
            Matcher makeMatch = readDigits.matcher(input);
            makeMatch.find();
            input = makeMatch.group();
            //output = Integer.parseInt(input);
            output = "50";
        } else if (input.contains(oneRow)) {
            System.out.println("One row");
            //Pattern readDigits = Pattern.compile("\\d+");
            //Matcher makeMatch = readDigits.matcher(input);
            //makeMatch.find();
            //input = makeMatch.group();
            //output = Integer.parseInt(input);
            output = "1";
        } else {
            System.out.println("unknown");
        }
        return output;
    }

/*	private static ArrayList<String> ReadSelect(WebDriver driver, String locator) throws Exception {
		ArrayList<String> output = new ArrayList<String>();
		int count = LocateElement.XpathCount(driver, locator, 2);
		if (count > 0) {
			WebElement element;
			for (int i = 1; i <= count; i++) {
				element = driver.findElement(By.xpath("("+locator+"/option)["+i+"]"));
				output.add(element.getText());
			}
			return output;
		} else {
			throw new NoOptionsException(locator);
		}
	}*/
}

/*class NoOptionsException extends Exception {

	NoOptionsException(String locator) {

		super("Locator: "+locator);
	}
}*/
