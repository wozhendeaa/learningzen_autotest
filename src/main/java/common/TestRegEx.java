package common;

import common.*;

/**
 * Created by miker on 5/27/2016.
 */
import java.util.regex.*;

public class TestRegEx {

    public static void main(String[] args) {

        // This class is used only for building and testing regular expressions
        // Do not externally call this class or depend on it, because it will be modified frequently

        System.out.println(Chronos.PastMilitaryTime(480));

        System.out.println(Chronos.PastTime(300)+" "+Chronos.PastMeridiem(300));

        System.exit(0);

        String lookAtThis;
        //lookAtThis = "d This is a, city 34 1 name, ST 2"
        //lookAtThis = "2,589 items found, displaying all items.";
        //String whatIGot = ParseCity(lookAtThis);
        //int whatIGot = ParseDigits(lookAtThis);
        //lookAtThis = "2,589 items found, displaying all items.";
        lookAtThis = "		case 123 :";
        String whatIGot = CaseNumbers(lookAtThis);
        System.out.println(whatIGot);
    }

    private static String ParseCity(String input) {
        String output = "";
        Pattern p = Pattern.compile(", [a-zA-Z]{2}$");
        Matcher m = p.matcher(input);
        if (m.find()) {
            String end = m.group();
            String[] start = input.split(end);
            output = start[0];
        } else {
            output = input;
        }
        return output;
    }

    private static int ParseDigits(String input) {
        input = input.replaceAll(",", "");
        int output = 0;
        String single = " items found displaying all items.";
        String multi = " items found displaying 1 to ";
        if (input.contains(single)) {
            System.out.println("single");
            Pattern readDigits = Pattern.compile("\\d+");
            Matcher makeMatch = readDigits.matcher(input);
            makeMatch.find();
            input = makeMatch.group();
            output = Integer.parseInt(input);
        } else if (input.contains(multi)) {
            System.out.println("multi");
            Pattern readDigits = Pattern.compile("\\d+");
            Matcher makeMatch = readDigits.matcher(input);
            makeMatch.find();
            input = makeMatch.group();
            output = Integer.parseInt(input);
        } else {
            System.out.println("unknown");
        }
        return output;
    }

    private static String Alphanumeric(String input) {
        String output = input;
        Pattern read = Pattern.compile("[^0-9a-zA-Z]");
        Matcher nonAlphanumeric = read.matcher(input);
        nonAlphanumeric.find();
        output = nonAlphanumeric.replaceAll(" ");
        return output;
    }

    private static String Alphabetic(String input) {
        String output = input;
        Pattern read = Pattern.compile("[^a-zA-Z]");
        Matcher nonAlphabetic = read.matcher(input);
        nonAlphabetic.find();
        output = nonAlphabetic.replaceAll(" ");
        return output;
    }

    private static String SingleCharacters(String input) {
        input = " "+input+" ";
        String output = input;
        Pattern read = Pattern.compile(" [0-9a-zA-Z] ");
        Matcher single = read.matcher(input);
        single.find();
        output = single.replaceAll(" ");
        return output;
    }

    private static String CaseNumbers(String input) {
        input = " "+input+" ";
        String output = input;
        Pattern read = Pattern.compile("\\s*case\\s*\\d+\\s*:\\s*");
        Matcher numbers = read.matcher(input);
        numbers.find();
        output = numbers.replaceAll("case :");
        return output;
    }
}
