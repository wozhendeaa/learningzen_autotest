package common;

/**
 * Created by miker on 5/27/2016.
 */
import java.io.File;
import java.util.ArrayList;

public class Directory {

    public ArrayList<String> List(String input, boolean folders) throws Exception {

        File folder = new File(input);
        File[] listOfFiles = folder.listFiles();

        ArrayList<String> output = new ArrayList<String>();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile() && folders == false) {
                //System.out.println("File " + listOfFiles[i].getName());
                output.add(listOfFiles[i].getName());
            } else if (listOfFiles[i].isDirectory() && folders == true) {
                //System.out.println("Directory " + listOfFiles[i].getName());
                output.add(listOfFiles[i].getName());
            }
        }

        if (output.isEmpty()) {
            throw new NothingFoundException(input, folders);
        }

        return output;
    }
}

class NothingFoundException extends Exception {

    NothingFoundException(String input, boolean folders) {

        super("Folders: "+folders+" ("+input+")");
    }
}
