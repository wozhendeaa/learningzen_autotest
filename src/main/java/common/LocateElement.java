package common;

/**
 * Created by miker on 5/27/2016.
 */
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LocateElement {

    /*	public static boolean ClassName (WebDriver driver, String locator, int timeout) {
            try {
                System.out.println("Find "+locator);
                WebElement element = driver.findElement(By.className(locator));
                if (element.isDisplayed()) {
                    return true;
                }
            } catch (Exception e1) {
                for (int i = 1; i < timeout*8; i += 1) {
                    try {
                        Thread.sleep(125);
                        WebElement element = driver.findElement(By.className(locator));
                        if (element.isDisplayed()) {
                            return true;
                        }
                    } catch (Exception e2) {
                        e1 = e2;
                    }
                }
                System.out.println(e1);
            }
            return false;
        }
    */
    public static String ClassName(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Find "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            int retry = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                retry = i;
                elementList = driver.findElements(By.className(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else if (elementCount != 1) {
                    break;
                } else {
                    element = driver.findElement(By.className(locator));
                    if (element.isDisplayed() && element.isEnabled()) {
                        return Integer.toString(elementCount);
                    } else {
                        // loop
                    }
                }
                Thread.sleep(125);
            }
            if (retry < timeout*8 - 1) {
                // continue
            } else {
                System.out.println("Cannot interact with: "+locator);
            }
            return Integer.toString(elementCount);
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    public static String ClassNameCount(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Count "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                elementList = driver.findElements(By.className(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else {
                    System.out.println("Counted elements: "+elementCount);
                    return Integer.toString(elementCount);
                }
                Thread.sleep(125);
            }
            return "0";
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    /*	public static boolean CssSelector (WebDriver driver, String locator, int timeout) {
            try {
                System.out.println("Find "+locator);
                WebElement element = driver.findElement(By.cssSelector(locator));
                if (element.isDisplayed()) {
                    return true;
                }
            } catch (Exception e1) {
                for (int i = 1; i < timeout*8; i += 1) {
                    try {
                        Thread.sleep(125);
                        WebElement element = driver.findElement(By.cssSelector(locator));
                        if (element.isDisplayed()) {
                            return true;
                        }
                    } catch (Exception e2) {
                        e1 = e2;
                    }
                }
                System.out.println(e1);
            }
            return false;
        }
    */
    public static String CssSelector(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Find "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            int retry = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                retry = i;
                elementList = driver.findElements(By.cssSelector(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else if (elementCount != 1) {
                    break;
                } else {
                    element = driver.findElement(By.cssSelector(locator));
                    if (element.isDisplayed() && element.isEnabled()) {
                        return Integer.toString(elementCount);
                    } else {
                        // loop
                    }
                }
                Thread.sleep(125);
            }
            if (retry < timeout*8 - 1) {
                // continue
            } else {
                System.out.println("Cannot interact with: "+locator);
            }
            return Integer.toString(elementCount);
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    public static String CssSelectorCount(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Count "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                elementList = driver.findElements(By.cssSelector(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else {
                    System.out.println("Counted elements: "+elementCount);
                    return Integer.toString(elementCount);
                }
                Thread.sleep(125);
            }
            return "0";
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    /*	public static boolean Id (WebDriver driver, String locator, int timeout) {
            try {
                System.out.println("Find "+locator);
                WebElement element = driver.findElement(By.id(locator));
                if (element.isDisplayed()) {
                    return true;
                }
            } catch (Exception e1) {
                for (int i = 1; i < timeout*8; i += 1) {
                    try {
                        Thread.sleep(125);
                        WebElement element = driver.findElement(By.id(locator));
                        if (element.isDisplayed()) {
                            return true;
                        }
                    } catch (Exception e2) {
                        e1 = e2;
                    }
                }
                System.out.println(e1);
            }
            return false;
        }
    */
    public static String Id(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Find "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            int retry = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                retry = i;
                elementList = driver.findElements(By.id(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else if (elementCount != 1) {
                    break;
                } else {
                    element = driver.findElement(By.id(locator));
                    if (element.isDisplayed() && element.isEnabled()) {
                        return Integer.toString(elementCount);
                    } else {
                        // loop
                    }
                }
                Thread.sleep(125);
            }
            if (retry < timeout*8 - 1) {
                // continue
            } else {
                System.out.println("Cannot interact with: "+locator);
            }
            return Integer.toString(elementCount);
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    public static String IdCount(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Count "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                elementList = driver.findElements(By.id(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else {
                    System.out.println("Counted elements: "+elementCount);
                    return Integer.toString(elementCount);
                }
                Thread.sleep(125);
            }
            return "0";
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    /*	public static boolean LinkText (WebDriver driver, String locator, int timeout) {
            try {
                System.out.println("Find "+locator);
                WebElement element = driver.findElement(By.linkText(locator));
                if (element.isDisplayed()) {
                    return true;
                }
            } catch (Exception e1) {
                for (int i = 1; i < timeout*8; i += 1) {
                    try {
                        Thread.sleep(125);
                        WebElement element = driver.findElement(By.linkText(locator));
                        if (element.isDisplayed()) {
                            return true;
                        }
                    } catch (Exception e2) {
                        e1 = e2;
                    }
                }
                System.out.println(e1);
            }
            return false;
        }
    */
    public static String LinkText(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Find "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            int retry = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                retry = i;
                elementList = driver.findElements(By.linkText(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else if (elementCount != 1) {
                    break;
                } else {
                    element = driver.findElement(By.linkText(locator));
                    if (element.isDisplayed() && element.isEnabled()) {
                        return Integer.toString(elementCount);
                    } else {
                        // loop
                    }
                }
                Thread.sleep(125);
            }
            if (retry < timeout*8 - 1) {
                // continue
            } else {
                System.out.println("Cannot interact with: "+locator);
            }
            return Integer.toString(elementCount);
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    public static String LinkTextCount(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Count "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                elementList = driver.findElements(By.linkText(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else {
                    System.out.println("Counted elements: "+elementCount);
                    return Integer.toString(elementCount);
                }
                Thread.sleep(125);
            }
            return "0";
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    /*	public static boolean Name (WebDriver driver, String locator, int timeout) {
            try {
                System.out.println("Find "+locator);
                WebElement element = driver.findElement(By.name(locator));
                if (element.isDisplayed()) {
                    return true;
                }
            } catch (Exception e1) {
                for (int i = 1; i < timeout*8; i += 1) {
                    try {
                        Thread.sleep(125);
                        WebElement element = driver.findElement(By.name(locator));
                        if (element.isDisplayed()) {
                            return true;
                        }
                    } catch (Exception e2) {
                        e1 = e2;
                    }
                }
                System.out.println(e1);
            }
            return false;
        }
    */
    public static String Name(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Find "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            int retry = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                retry = i;
                elementList = driver.findElements(By.name(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else if (elementCount != 1) {
                    break;
                } else {
                    element = driver.findElement(By.name(locator));
                    if (element.isDisplayed() && element.isEnabled()) {
                        return Integer.toString(elementCount);
                    } else {
                        // loop
                    }
                }
                Thread.sleep(125);
            }
            if (retry < timeout*8 - 1) {
                // continue
            } else {
                System.out.println("Cannot interact with: "+locator);
            }
            return Integer.toString(elementCount);
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    public static String NameCount(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Count "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                elementList = driver.findElements(By.name(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else {
                    System.out.println("Counted elements: "+elementCount);
                    return Integer.toString(elementCount);
                }
                Thread.sleep(125);
            }
            return "0";
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    /*	public static boolean PartialLinkText (WebDriver driver, String locator, int timeout) {
            try {
                System.out.println("Find "+locator);
                WebElement element = driver.findElement(By.partialLinkText(locator));
                if (element.isDisplayed()) {
                    return true;
                }
            } catch (Exception e1) {
                for (int i = 1; i < timeout*8; i += 1) {
                    try {
                        Thread.sleep(125);
                        WebElement element = driver.findElement(By.partialLinkText(locator));
                        if (element.isDisplayed()) {
                            return true;
                        }
                    } catch (Exception e2) {
                        e1 = e2;
                    }
                }
                System.out.println(e1);
            }
            return false;
        }
    */
    public static String PartialLinkText(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Find "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            int retry = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                retry = i;
                elementList = driver.findElements(By.partialLinkText(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else if (elementCount != 1) {
                    break;
                } else {
                    element = driver.findElement(By.partialLinkText(locator));
                    if (element.isDisplayed() && element.isEnabled()) {
                        return Integer.toString(elementCount);
                    } else {
                        // loop
                    }
                }
                Thread.sleep(125);
            }
            if (retry < timeout*8 - 1) {
                // continue
            } else {
                System.out.println("Cannot interact with: "+locator);
            }
            return Integer.toString(elementCount);
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    public static String PartialLinkTextCount(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Count "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                elementList = driver.findElements(By.partialLinkText(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else {
                    System.out.println("Counted elements: "+elementCount);
                    return Integer.toString(elementCount);
                }
                Thread.sleep(125);
            }
            return "0";
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    /*	public static boolean TagName (WebDriver driver, String locator, int timeout) {
            try {
                System.out.println("Find "+locator);
                WebElement element = driver.findElement(By.tagName(locator));
                if (element.isDisplayed()) {
                    return true;
                }
            } catch (Exception e1) {
                for (int i = 1; i < timeout*8; i += 1) {
                    try {
                        Thread.sleep(125);
                        WebElement element = driver.findElement(By.tagName(locator));
                        if (element.isDisplayed()) {
                            return true;
                        }
                    } catch (Exception e2) {
                        e1 = e2;
                    }
                }
                System.out.println(e1);
            }
            return false;
        }
    */
    public static String TagName(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Find "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            int retry = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                retry = i;
                elementList = driver.findElements(By.tagName(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else if (elementCount != 1) {
                    break;
                } else {
                    element = driver.findElement(By.tagName(locator));
                    if (element.isDisplayed() && element.isEnabled()) {
                        return Integer.toString(elementCount);
                    } else {
                        // loop
                    }
                }
                Thread.sleep(125);
            }
            if (retry < timeout*8 - 1) {
                // continue
            } else {
                System.out.println("Cannot interact with: "+locator);
            }
            return Integer.toString(elementCount);
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    public static String TagNameCount(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Count "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                elementList = driver.findElements(By.tagName(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else {
                    System.out.println("Counted elements: "+elementCount);
                    return Integer.toString(elementCount);
                }
                Thread.sleep(125);
            }
            return "0";
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    /*	public static boolean Xpath (WebDriver driver, String locator, int timeout) {
            try {
                System.out.println("Find "+locator);
                WebElement element = driver.findElement(By.xpath(locator));
                if (element.isDisplayed()) {
                    return true;
                }
            } catch (Exception e1) {
                for (int i = 1; i < timeout*8; i += 1) {
                    try {
                        Thread.sleep(125);
                        WebElement element = driver.findElement(By.xpath(locator));
                        if (element.isDisplayed()) {
                            return true;
                        }
                    } catch (Exception e2) {
                        e1 = e2;
                    }
                }
                System.out.println(e1);
            }
            return false;
        }
    */
    public static String Xpath(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Find "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            int retry = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                retry = i;
                elementList = driver.findElements(By.xpath(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else if (elementCount != 1) {
                    break;
                } else {
                    element = driver.findElement(By.xpath(locator));
                    if (element.isDisplayed() && element.isEnabled()) {
                        return Integer.toString(elementCount);
                    } else {
                        // loop
                    }
                }
                Thread.sleep(125);
            }
            if (retry < timeout*8 - 1) {
                // continue
            } else {
                System.out.println("Cannot interact with: "+locator);
            }
            return Integer.toString(elementCount);
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }

    public static String XpathCount(WebDriver driver, String locator, String bindingStage, int timeout) {

        try {
            System.out.println("Count "+locator);
            List<WebElement> elementList;
            int elementCount = 0;
            WebElement element;
            for (int i = 1; i < timeout*8; i += 1) {
                elementList = driver.findElements(By.xpath(locator));
                elementCount = elementList.size();
                if (elementCount == 0) {
                    // loop
                } else {
                    System.out.println("Counted elements: "+elementCount);
                    return Integer.toString(elementCount);
                }
                Thread.sleep(125);
            }
            return "0";
        } catch (Exception all) {
            ScreenShot.Save(driver, bindingStage, ""+all);
            return ""+all;
        }
    }
}

class NonexistantElementException extends Exception {

    NonexistantElementException(int count) {

        super("Counted elements: "+count);
    }
}

class UnusableElementException extends Exception {

    UnusableElementException(int count) {

        super("Counted elements: "+count);
    }
}

class ElementCountException extends Exception {

    ElementCountException(int count) {

        super("Counted elements: "+count);
    }
}

