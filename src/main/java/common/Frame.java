package common;

/**
 * Created by miker on 5/27/2016.
 */
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Frame {

    public static boolean Top(WebDriver driver) {

        // Switch to the default frame
        driver.switchTo().defaultContent();

        // See if there are any frame elements
        if (driver.findElements(By.tagName("frame")).isEmpty()) {
            // Do nothing
        } else {

            // Store the content of the default frame
            String defaultSource = driver.getPageSource();

            String parentSource = "initialize";

            // Check that the content of the default frame is not null
            while (!defaultSource.equals(null)) {

                // Switch to the default frame
                driver.switchTo().parentFrame();

                // Store the content of the parent frame
                parentSource = driver.getPageSource();

                // See if the content of the default frame and the parent frame are the same
                if (defaultSource.equals(parentSource)) {
                    return true;

                }

            }

        }

        // Stop because there are no frame elements
        return false;

    }

    public static boolean Find(WebDriver driver, String locator) {

        // Select the top most frame
        Top(driver);

        String frameLevelOne = "initialize";
        String frameLevelTwo = "initialize";
        for (int i = 1; i <= driver.findElements(By.tagName("frame")).size(); i += 1) {
            frameLevelOne = driver.findElement(By.xpath("(//frame)["+i+"]")).getAttribute("name");
            if (frameLevelOne.equals(locator)) {
                driver.switchTo().frame(locator);
                System.out.println(locator+" frame selected");
                return true;
            } else {
                driver.switchTo().frame(frameLevelOne);
                for (int j = 1; j <= driver.findElements(By.tagName("frame")).size(); j++) {
                    frameLevelTwo = driver.findElement(By.xpath("(//frame)["+j+"]")).getAttribute("name");
                    if (frameLevelTwo.equals(locator)) {
                        driver.switchTo().frame(locator);
                        System.out.println(locator+" frame selected");
                        return true;
                    }
                }

                // Select the top most frame
                Top(driver);

            }

        }

        // Stop because there are no matching frame elements
        return false;

    }

}
