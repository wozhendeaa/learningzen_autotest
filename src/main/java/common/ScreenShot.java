package common;

/**
 * Created by miker on 5/27/2016.
 */
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class ScreenShot {

    public static boolean Save(WebDriver driver, String ouputDirectory, String message) {

        try {
            String filePath = ouputDirectory+"\\"+DateTime.DateStampName()+"\\"+DateTime.TimeStampName();
            File screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenShot, new File(filePath+".png"));

            File outFile = new File (filePath+".txt");
            FileWriter fWriter = null;
            fWriter = new FileWriter (outFile);
            PrintWriter pWriter = new PrintWriter (fWriter);
            pWriter.print(message);
            pWriter.flush();
            pWriter.close();
            System.out.println("Save screenshot: "+filePath);
            return true;
        } catch (Exception e) {
            //throw new ScreenShotExeption();
            return false;
        }
    }

}

class ScreenShotExeption extends Exception {

    ScreenShotExeption() {

        super("Screenshot error");
    }
}