package common;

/**
 * Created by miker on 5/27/2016.
 */

public class PackageManifest {

    // The purpose of this is to facilitate using a standard Windows directory structure
    // This is needed because these projects depend on multiple external binaries, JAR files, and scripts
    // Not following this structure can cause access rights to interfere with execution
    // The project and all dependencies should be within %UserProfile%

public PackageManifest() {

    }

    public String[] Integration() {

        // These are directory names that are used within the project
        String[] folderArray = {
                this.localConfigPath, "Run",
                this.localConfigFilePath, "local_uitesting_config.txt",
                this.autoit, "AutoIt3",
                this.marionette, "geckodriver-v0.8.0-win32",
                this.iexplore, "IEDriverServer_Win32_2.53.1",
                this.phantom, "phantomjs-2.1.1-windows",
                this.chrome, "chromedriver_win32_2.21",
                this.opera, "operadriver_win32_0.2.2",
                this.edge, "Microsoft Web Driver", // 10586
        };

        String userPath = System.getProperty("user.home");
        String systemDrive = "";
        try {
            PathTool path = new PathTool();
            systemDrive = path.Split(userPath, 1);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        ArraySplitter name = new ArraySplitter();

        String configDir = name.Key(this.localConfigPath, folderArray);
        String coinfigFile = name.Key(this.localConfigFilePath, folderArray);

        ArrayFromTextFile configuration = new ArrayFromTextFile();
        String[] configArray = configuration.Read(userPath+"\\"+configDir+"\\"+coinfigFile);
        ArraySplitter configSetting = new ArraySplitter();
        String projectPath = configSetting.Key("Local_Path_To_Project", configArray);

        String autoIt3Dir = name.Key(this.autoit, folderArray);
        String marionetteDir = name.Key(this.marionette, folderArray);
        String iexploreDir = name.Key(this.iexplore, folderArray);
        String phantomDir = name.Key(this.phantom, folderArray);
        String chromeDir = name.Key(this.chrome, folderArray);
        String operaDir = name.Key(this.opera, folderArray);
        String edgeDir = name.Key(this.edge, folderArray);

        // These are the full directory paths that will be verified
        // If a dependency listed here is not installed AND is not needed, comment it out
        String[] pathArray = {
                this.localConfigPath, userPath+"\\"+configDir,
                this.localConfigFilePath, userPath+"\\"+configDir+"\\"+coinfigFile,
                this.project, projectPath,
                this.autoit, projectPath+"\\autoit_binary\\"+autoIt3Dir+"\\AutoIt3.exe",
                this.marionette, projectPath+"\\webdriver_binary\\"+marionetteDir+"\\geckodriver.exe",
                this.iexplore, projectPath+"\\webdriver_binary\\"+iexploreDir+"\\IEDriverServer.exe",
                this.phantom, projectPath+"\\webdriver_binary\\"+phantomDir+"\\bin\\phantomjs.exe",
                this.chrome, projectPath+"\\webdriver_binary\\"+chromeDir+"\\chromedriver.exe",
                this.opera, projectPath+"\\webdriver_binary\\"+operaDir+"\\operadriver.exe",
                this.edge, systemDrive+"\\Program Files (x86)\\"+edgeDir+"\\MicrosoftWebDriver.exe",
        };

        try {
            PathTool list = new PathTool();
            list.Checker(pathArray);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        return pathArray;
    }

    // These are the unique key names that identify a correlating element of data within an array
    public static String localConfigPath = "Local_Config_Directory";
    public static String localConfigFilePath = "Local_Config_File_Path";
    public static String project = "Configured_Local_Project_Path";
    public static String autoit = "AutoIt3_Binary_Dir";
    public static String marionette = "Marionette_Driver_Directory";
    public static String iexplore = "Internet_Explorer_Driver_Directory";
    public static String phantom = "PhantomJS_Driver_Directory";
    public static String chrome = "Google_Chrome_Driver_Directory";
    public static String opera = "Opera_Driver_Directory";
    public static String edge = "Edge_Driver_Directory";

}
