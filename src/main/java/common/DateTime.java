package common;

/**
 * Created by miker on 5/27/2016.
 */
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DateTime {

    private static String Fix(int input, int digits) {

        String output = Integer.toString(input);
        for (int i = digits; output.length() < i;) {
            output = "0"+output;
        }
        return output;
    }

    public static String Current() {
        Calendar today = new GregorianCalendar();

        String year = DateTime.Fix(today.get(Calendar.YEAR), 2);
        String month = DateTime.Fix((today.get(Calendar.MONTH))+1, 2);
        String dayOfMonth = DateTime.Fix(today.get(Calendar.DAY_OF_MONTH), 2);
        String hourOfDay = DateTime.Fix(today.get(Calendar.HOUR_OF_DAY), 2);
        String minute = DateTime.Fix(today.get(Calendar.MINUTE), 2);
        String second = DateTime.Fix(today.get(Calendar.SECOND), 2);
        String milli = DateTime.Fix(today.get(Calendar.MILLISECOND), 3);
        TimeZone zone = today.getTimeZone();
        int offset = zone.getRawOffset();
        int savings = zone.getDSTSavings();
        String utc = Integer.toString((savings+offset)/1000/60/60);

        String output = year+"-"+month+"-"+dayOfMonth+" "+hourOfDay+":"+minute+":"+second+"."+milli+" UTC"+utc;

        return output;
    }

    public static String CurrentFullFileName() {
        Calendar today = new GregorianCalendar();

        String year = DateTime.Fix(today.get(Calendar.YEAR), 2);
        String month = DateTime.Fix((today.get(Calendar.MONTH))+1, 2);
        String dayOfMonth = DateTime.Fix(today.get(Calendar.DAY_OF_MONTH), 2);
        String hourOfDay = DateTime.Fix(today.get(Calendar.HOUR_OF_DAY), 2);
        String minute = DateTime.Fix(today.get(Calendar.MINUTE), 2);
        String second = DateTime.Fix(today.get(Calendar.SECOND), 2);
        String milli = DateTime.Fix(today.get(Calendar.MILLISECOND), 3);
        TimeZone zone = today.getTimeZone();
        int offset = zone.getRawOffset();
        int savings = zone.getDSTSavings();
        String utc = Integer.toString((savings+offset)/1000/60/60);

        String output = year+"-"+month+"-"+dayOfMonth+"_"+hourOfDay+minute+"_"+second+"."+milli+"_UTC"+utc;

        return output;
    }

    public static String TimeStampName() {
        Calendar today = new GregorianCalendar();

        String year = DateTime.Fix(today.get(Calendar.YEAR), 2);
        String month = DateTime.Fix((today.get(Calendar.MONTH))+1, 2);
        String dayOfMonth = DateTime.Fix(today.get(Calendar.DAY_OF_MONTH), 2);
        String hourOfDay = DateTime.Fix(today.get(Calendar.HOUR_OF_DAY), 2);
        String minute = DateTime.Fix(today.get(Calendar.MINUTE), 2);
        String second = DateTime.Fix(today.get(Calendar.SECOND), 2);
        String milli = DateTime.Fix(today.get(Calendar.MILLISECOND), 3);
        TimeZone zone = today.getTimeZone();
        int offset = zone.getRawOffset();
        int savings = zone.getDSTSavings();
        String utc = Integer.toString((savings+offset)/1000/60/60);

        String output = hourOfDay+minute+"_"+second+"."+milli+"_UTC"+utc;

        return output;
    }

    public static String DateStampName() {
        Calendar today = new GregorianCalendar();

        String year = DateTime.Fix(today.get(Calendar.YEAR), 2);
        String month = DateTime.Fix((today.get(Calendar.MONTH))+1, 2);
        String dayOfMonth = DateTime.Fix(today.get(Calendar.DAY_OF_MONTH), 2);
        String hourOfDay = DateTime.Fix(today.get(Calendar.HOUR_OF_DAY), 2);
        String minute = DateTime.Fix(today.get(Calendar.MINUTE), 2);
        String second = DateTime.Fix(today.get(Calendar.SECOND), 2);
        String milli = DateTime.Fix(today.get(Calendar.MILLISECOND), 3);
        TimeZone zone = today.getTimeZone();
        int offset = zone.getRawOffset();
        int savings = zone.getDSTSavings();
        String utc = Integer.toString((savings+offset)/1000/60/60);

        String output = year+"-"+month+"-"+dayOfMonth;

        return output;
    }

    public static String CurrentDayWithZero() {
        Calendar today = new GregorianCalendar();

        String dayOfMonth = DateTime.Fix(today.get(Calendar.DAY_OF_MONTH), 2);

        String output = dayOfMonth;

        return output;
    }

    public static String CurrentMonthWithZero() {
        Calendar today = new GregorianCalendar();

        String month = DateTime.Fix((today.get(Calendar.MONTH))+1, 2);

        String output = month;

        return output;
    }

    public static String CurrentYear() {
        Calendar today = new GregorianCalendar();

        String year = DateTime.Fix(today.get(Calendar.YEAR), 2);;

        String output = year;

        return output;
    }
}
