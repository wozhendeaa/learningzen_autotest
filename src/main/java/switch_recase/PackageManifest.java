package switch_recase;

/**
 * Created by miker on 5/27/2016.
 */
import java.io.File;
import common.*;

class PackageManifest {

    // The purpose of this is to facilitate using a standard Windows directory structure
    // This is needed because these projects depend on multiple external binaries, JAR files, and scripts
    // Not following this structure can cause access rights to interfere with execution
    // The project and all dependencies should be within %UserProfile%

    PackageManifest() {

    }

    String[] Integration() {

        // These are static directory names for external dependencies
        String[] folderArray = {
                this.deploy, "Run",
                this.manifest, "UiSwitchRecase",
        };

        String userPath = System.getProperty("user.home");
        String publicPath = SplitSlash(userPath);

        ArraySplitter name = new ArraySplitter();

        String deployDir = name.Key(this.deploy, folderArray);
        String packageDir = name.Key(this.manifest, folderArray);

        // These are dynamic full directory paths for external dependencies
        // If a dependency listed here is not installed AND is not needed, comment it out
        String[] pathArray = {
                this.manifest, userPath+"\\"+deployDir+"\\"+packageDir,
        };

        try {
            PathTool list = new PathTool();
            list.Checker(pathArray);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        return pathArray;
    }

    private String SplitSlash(String input) {

        String[] parts = input.split("\\\\");

        String output = parts[0];
        for (int i = 1; i < parts.length - 1 ; i += 1) {
            output = output+"\\"+parts[i];
        }
        return output+"\\Public";

    }

    // These are the unique key names that identify a correlating element of data within an array
    final static String deploy = "deployDir";
    final static String manifest = "packageDir";

}
