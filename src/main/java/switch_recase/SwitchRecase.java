package switch_recase;

/**
 * Created by miker on 5/27/2016.
 */
import common.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.*;

public class SwitchRecase {

    // The purpose of this is to correct the case numbering in a java switch{case}

    public final static PackageManifest manifest = new PackageManifest();
    public final static String[] pathArray = manifest.Integration();
    public final static ArraySplitter path = new ArraySplitter();

    public final static String packagePath = path.Key(PackageManifest.manifest, pathArray);

    public static void main(String[] args) {
        try{

            // Choose a java switch/case method
            String sourcePath = packagePath+"\\BrokenSwitch.txt";
            String resultPath = packagePath+"\\CorrectedSwitch.txt";

            // Create a buffered input stream reader
            FileInputStream sourceInputLength = new FileInputStream(sourcePath);
            InputStreamReader sourceStreamLength = new InputStreamReader(sourceInputLength);
            BufferedReader sourceBufferLength = new BufferedReader(sourceStreamLength);

            // Count the lines and create an array
            long sourceLengthLong = sourceBufferLength.lines().count();
            int sourceLength = Math.round(sourceLengthLong);
            sourceInputLength.close();

            // Create a buffered input stream reader
            FileInputStream sourceInput = new FileInputStream(sourcePath);
            InputStreamReader sourceStream = new InputStreamReader(sourceInput);
            BufferedReader sourceBuffer = new BufferedReader(sourceStream);

            // Read the lines into the array
            String[] source = new String[sourceLength];
            for (int i = 0; i < sourceLength; i += 1) {
                source[i] = sourceBuffer.readLine();
            }
            sourceInput.close();

            // Create another array of the same size
            String[] result = new String[sourceLength];

            // Copy the first array into the second, and perform RegEx translations
            for (int i = 0; i < sourceLength; i += 1) {
                result[i] = CaseNumbers(source[i]);
            }

            // Create a buffered file writer
            FileWriter resultFile = new FileWriter(resultPath, false);
            BufferedWriter resultBuffer = new BufferedWriter(resultFile);

            // Write the translated array to a file
            for (int i = 0; i < sourceLength; i += 1) {
                System.out.println(result[i]);
                resultBuffer.write(result[i]);
                resultBuffer.newLine();
            }
            resultBuffer.flush();
            resultFile.close();

        }catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    static int newCaseNumber = 0;

    private static String CaseNumbers(String input) {
        String output = input;
        Pattern read = Pattern.compile("case\\s*\\d+\\s*:\\s*");
        Matcher caseNumber = read.matcher(input);
        if (caseNumber.find()) {
            newCaseNumber += 1;
            output = caseNumber.replaceAll("case "+newCaseNumber+":");
        }
        return output;
    }
}
