package learningzen.usecase_tests;

/**
 * Created by miker on 5/27/2016.
 */
import common.*;

import java.util.Date;

import learningzen.usecase_tests.sequences.LoginTests.Login;
import org.openqa.selenium.WebDriver;

public class LearningZenUseCaseTests {

    public static void main(String[] args) {

        PackageManifest manifest = new PackageManifest();
        String[] pathArray = manifest.Integration();

        ArraySplitter path = new ArraySplitter();
        String projectPath = path.Key(PackageManifest.project, pathArray);
        String localConfigPath = path.Key(PackageManifest.localConfigPath, pathArray);

        String configFilePath = projectPath+"\\src\\main\\java\\learningzen\\usecase_tests\\Config.txt";
        ArrayFromTextFile configFile = new ArrayFromTextFile();
        String[] configArray = configFile.Read(configFilePath);
        ArraySplitter config = new ArraySplitter();

        String currentStage = config.Key("Current_Stage", configArray);
        String currentDriver = config.Key("Current_Driver", configArray);
        String logName = config.Key("Log_File_Name", configArray);

        String stageFilePath = projectPath+"\\src\\main\\java\\learningzen\\usecase_tests\\"+currentStage+"Stage.txt";
        ArrayFromTextFile stageFile = new ArrayFromTextFile();
        String[] stageArray = stageFile.Read(stageFilePath);
        ArraySplitter stage = new ArraySplitter();

        WebDriver driver = WebControl.StartDriver(currentDriver, pathArray);

        Date start = null;
        Date finish = null;
        long elapsed = 0;
        int chain = 0;
        String status = null;

        chain = 1;
        while (chain > 0) {
            switch (chain) {
                case 1:

                    start = new Date();
                    status = Login.As(driver, "Site_Administrator", pathArray);
                    finish = new Date();
                    elapsed = Math.round((finish.getTime() - start.getTime()) / 1000);

                    if (status.equals("error")) {
                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage, "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - Login.As Site_Administrator ERROR (" + elapsed + " seconds)");
                    } else {
                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage, "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - Login.As Site_Administrator OK (" + elapsed + " seconds)");
                    }

                    chain += 1;
                    break;
                case 2:

                    start = new Date();
                    status = Login.As(driver, "Portal_Admin", pathArray);
                    finish = new Date();
                    elapsed = Math.round((finish.getTime() - start.getTime()) / 1000);

                    if (status.equals("error")) {
                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage, "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - Login.As Portal_Admin ERROR (" + elapsed + " seconds)");
                    } else {
                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage, "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - Login.As Portal_Admin OK (" + elapsed + " seconds)");
                    }

                    chain += 1;
                    break;
                case 3:

//                    start = new Date();
//                    status = Login.As(driver, "Portal_Manager", pathArray);
//                    finish = new Date();
//                    elapsed = Math.round((finish.getTime() - start.getTime()) / 1000);
//
//                    if (status.equals("error")) {
//                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage, "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - Login.As Portal_Manager ERROR (" + elapsed + " seconds)");
//                    } else {
//                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage, "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - Login.As Portal_Manager OK (" + elapsed + " seconds)");
//                    }

                    chain += 1;
                    break;
                case 4:

//                    start = new Date();
//                    status = Login.As(driver, "Portal_Student", pathArray);
//                    finish = new Date();
//                    elapsed = Math.round((finish.getTime() - start.getTime()) / 1000);
//
//                    if (status.equals("error")) {
//                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage, "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - Login.As Portal_Student ERROR (" + elapsed + " seconds)");
//                    } else {
//                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage, "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - Login.As Portal_Student OK (" + elapsed + " seconds)");
//                    }

                    chain += 1;
                    break;
                case 5:

//                    start = new Date();
//                    status = SignUp.SignUp(driver, pathArray);
//                    finish = new Date();
//                    elapsed = Math.round((finish.getTime() - start.getTime()) / 1000);
//
//                    if (status.equals("error")) {
//                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage, "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - SignUp.SignUp ERROR (" + elapsed + " seconds)");
//                    } else {
//                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage, "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - SignUp.SignUp OK (" + elapsed + " seconds)");
//                    }

                    chain += 1;
                    break;
                case 6:

//                    start = new Date();
//                    status = RequestDemo.DemoRequest(driver, pathArray);
//                    finish = new Date();
//                    elapsed = Math.round((finish.getTime() - start.getTime()) / 1000);
//
//                    if (status.equals("error")) {
//                       Log.Save(localConfigPath+"\\LearningZen\\"+currentStage, "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - RequestDemo.DemoRequest ERROR (" + elapsed + " seconds)");
//                    } else {
//                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage, "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - RequestDemo.DemoRequest OK (" + elapsed + " seconds)");
//                    }
//
                    chain += 1;
                    break;
                case 7:

//                    start = new Date();
//                    status = CourseTest.CourseTest(driver, pathArray);
//                    finish = new Date();
//                    elapsed = Math.round((finish.getTime() - start.getTime()) / 1000);
//
//                    if (status.equals("error")) {
//                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage, "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - CourseTest.CourseTest ERROR (" + elapsed + " seconds)");
//                    } else {
//                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage, "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - CourseTest.CourseTest OK (" + elapsed + " seconds)");
//                    }

                    chain += 1;
                    break;
                case 8:

//                    start = new Date();
//                    status = CreateACourse.CreateACourse(driver, pathArray);
//                    finish = new Date();
//                    elapsed = Math.round((finish.getTime() - start.getTime()) / 1000);
//
//                    if (status.equals("error")) {
//                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage", "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - CreateACourse.CreateACourse ERROR (" + elapsed + " seconds)");
//                    } else {
//                        Log.Save(localConfigPath+"\\LearningZen\\"+currentStage", "\\" + logName, Chronos.CurrentDashDate() + " " + Chronos.CurrentTime() + " - CreateACourse.CreateACourse OK (" + elapsed + " seconds)");
//                    }

                    chain += 1;
                    break;
                default:
                    System.out.println("End of chain LearningZenUseCaseTests");
                    chain = 0;
                    break;
                }
            }
//        driver.close();
//        driver.quit();
        WebControl.SeleniumServerTerminator(args, pathArray);
        }
}
