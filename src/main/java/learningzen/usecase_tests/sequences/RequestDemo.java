package learningzen.usecase_tests.sequences;

/**
 * Created by gordonz on 6/15/2016.
 */

import java.util.ArrayList;
import java.util.List;

import common.*;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.Select;

public class RequestDemo {

    public static String DemoRequest(WebDriver driver, String[] pathArray) {

        ArraySplitter path = new ArraySplitter();
        String projectPath = path.Key(PackageManifest.project, pathArray);

        String configFilePath = projectPath+"\\src\\main\\java\\learningzen\\usecase_tests\\Config.txt";
        ArrayFromTextFile configFile = new ArrayFromTextFile();
        String[] configArray = configFile.Read(configFilePath);
        ArraySplitter config = new ArraySplitter();

        String currentStage = config.Key("Current_Stage", configArray);

        String stageFilePath = projectPath+"\\src\\main\\java\\learningzen\\usecase_tests\\"+currentStage+"Stage.txt";
        ArrayFromTextFile stageFile = new ArrayFromTextFile();
        String[] stageArray = stageFile.Read(stageFilePath);
        ArraySplitter stage = new ArraySplitter();

        String xpathMapPath = projectPath+"\\src\\main\\java\\learningzen\\usecase_tests\\XpathMap.txt";
        ArrayFromTextFile xpathMapFile = new ArrayFromTextFile();
        String[] mapArray = xpathMapFile.Read(xpathMapPath);
        ArraySplitter xpathMap = new ArraySplitter();

        String method = "DemoRequest";
        String output = null;
        int retry = 0;
        int maxRetry = 20;
        WebElement element;
        List<WebElement> elements;
        Select option = null;
        ArrayList<String> optionList = new ArrayList<String>();
        String text = null;

        String locator = null;

        int chain = 1;
        while (chain > 0) {
            try {
                switch (chain) {
                    case 1:

                        locator = stage.Key("LearningZen_Url", stageArray);
                        driver.get(locator);

                        retry = 0;
                        chain += 1;
                        break;
                    case 2:

                        int windowX = Integer.parseInt(config.Key("Driver_Window_Width", configArray));
                        int windowY = Integer.parseInt(config.Key("Driver_Window_Height", configArray));
                        driver.manage().window().setSize(new Dimension(windowX, windowY));

                        retry = 0;
                        chain += 1;
                        break;
                    case 3:

                        locator = xpathMap.Key("Request_Demo_Button_Xpath", mapArray);
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
                    case 4:

                        locator = xpathMap.Key("Name_Field_Xpath", mapArray);
                        element = driver.findElement(By.xpath(locator));
                        text = CreateText.RandomLetters(8, "first")+" GSTest";
                        element.sendKeys(text);

                        retry = 0;
                        chain += 1;
                        break;
                    case 5:

                        locator = xpathMap.Key("Email_Field_Xpath", mapArray);
                        element = driver.findElement(By.xpath(locator));
                        text = CreateText.RandomLetters(8, "lower")+"@gstest.mooo.com";
                        element.sendKeys(text);

                        retry = 0;
                        chain += 1;
                        break;
                    case 6:

                        locator = xpathMap.Key("Phone_Field_Xpath", mapArray);
                        element = driver.findElement(By.xpath(locator));
                        text = CreateText.RandomNumbers(3)+"-"+CreateText.RandomNumbers(3)+"-"+CreateText.RandomNumbers(4);
                        element.sendKeys(text);

                        retry = 0;
                        chain += 1;
                        break;
                    case 7:

                        locator = xpathMap.Key("Company_Field_Xpath", mapArray);
                        element = driver.findElement(By.xpath(locator));
                        text = CreateText.RandomLetters(8, "first")+" GSTest, Inc.";
                        element.sendKeys(text);

                        retry = 0;
                        chain += 1;
                        break;
                    case 8:

                        locator = xpathMap.Key("Message_Field_Xpath", mapArray);
                        element = driver.findElement(By.xpath(locator));
                        text = "This is only a test.";
                        element.sendKeys(text);

                        retry = 0;
                        chain += 1;
                        break;
                    case 9:

                        locator = xpathMap.Key("Submit_Button_Xpath", mapArray);
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
                    case 10:
//                    default:
                        output = "success"; // This is the last step in the chain, so that means it was successful
                        chain = 0; // This is the last step in the chain, so set the count to zero, and exit the error-catching loop
                        break;
                }
            } catch (UnhandledAlertException uae) {
                retry += 1;
                try {
                    Alert alert = driver.switchTo().alert();
                    text = alert.getText();
                    alert.dismiss();
                    System.out.println("Dismiss alert: " + text);
                } catch (NoAlertPresentException nape) {
                    System.out.println("No alert is active");
                }
            } catch (NoSuchElementException nsem) {
                retry += 1;
                if (retry > maxRetry) {
                    nsem.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
                text = driver.getTitle();
                if (text.toLowerCase().equals("certificate error: navigation blocked")) {

                    locator = "//a[@id='continueLink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.Edge(driver);
                    }

                    locator = "//a[@id='overridelink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.IE11(driver);
                    }
                }

                boolean firefoxBlock = true;
                while (firefoxBlock) {
                    text = driver.getTitle();
                    if (text.trim().toLowerCase().equals("insecure connection")) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    } else {
                        break;
                    }
                }

            } catch (NoSuchWindowException nswe) {
                retry += 1;
                if (retry > maxRetry) {
                    System.out.println("If this error is happening in Internet Explorer, check the Internet Properties: Security Zones. They should have the same \"Protected Mode\" check-boxes.");
                    System.out.println("If using IE11, make sure that the FEATURE_BFCACHE registry setting exists.");
                    nswe.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            } catch (UnreachableBrowserException ube) {
                retry += 1;
                if (retry > maxRetry) {
                    System.out.println("Make sure that the HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\DWORD(MaxUserPort, 65534) registry setting exists.");
                    ube.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            } catch (WebDriverException wde) {
                retry += 1;
                if (retry > maxRetry) {
                    wde.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
                text = driver.getTitle();
                if (text.toLowerCase().equals("certificate error: navigation blocked")) {
                    locator = "//a[@id='continueLink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.Edge(driver);
                    }

                    locator = "//a[@id='overridelink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.IE11(driver);
                    }
                }

                boolean firefoxBlock = true;
                while (firefoxBlock) {
                    text = driver.getTitle();
                    if (text.trim().toLowerCase().equals("insecure connection")) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    } else {
                        break;
                    }
                }
            } catch (Exception e) {
                retry += 1;
                if (retry > maxRetry) {
                    e.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        }
        return output;
    }
}
