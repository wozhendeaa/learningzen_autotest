package learningzen.usecase_tests.sequences;

/**
 * Created by gordonz on 6/21/2016.
 */
import java.util.ArrayList;
import java.util.List;

import common.*;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.Select;

public class CourseTest {

    public static String CourseTest(WebDriver driver, String[] pathArray) {

        ArraySplitter path = new ArraySplitter();
        String projectPath = path.Key(PackageManifest.project, pathArray);

        String configFilePath = projectPath+"\\src\\main\\java\\learningzen\\usecase_tests\\Config.txt";
        ArrayFromTextFile configFile = new ArrayFromTextFile();
        String[] configArray = configFile.Read(configFilePath);
        ArraySplitter config = new ArraySplitter();

        String currentStage = config.Key("Current_Stage", configArray);

        String stageFilePath = projectPath+"\\src\\main\\java\\learningzen\\usecase_tests\\"+currentStage+"Stage.txt";
        ArrayFromTextFile stageFile = new ArrayFromTextFile();
        String[] stageArray = stageFile.Read(stageFilePath);
        ArraySplitter stage = new ArraySplitter();

        String xpathMapPath = projectPath+"\\src\\main\\java\\learningzen\\usecase_tests\\XpathMap.txt";
        ArrayFromTextFile xpathMapFile = new ArrayFromTextFile();
        String[] mapArray = xpathMapFile.Read(xpathMapPath);
        ArraySplitter xpathMap = new ArraySplitter();

        String method = "CourseTest";
        String output = null;
        String locator = "";
        int retry = 0;
        int maxRetry = 20;
        WebElement element = null;
        List<WebElement> elements = null;
        Select option = null;
        ArrayList<String> optionList = new ArrayList<String>();
        String text = "";
        Actions builder = null;
        int exists = 0;

        int chain = 1;
        while (chain > 0) {
            try {
                switch (chain) {
                    case 1:

                        locator = stage.Key("LearningZen_Url", stageArray);
                        driver.get(locator);

                        retry = 0;
                        chain += 1;
                        break;
                    case 2:

                        int windowX = Integer.parseInt(config.Key("Driver_Window_Width", configArray));
                        int windowY = Integer.parseInt(config.Key("Driver_Window_Height", configArray));
                        driver.manage().window().setSize(new Dimension(windowX, windowY));

                        retry = 0;
                        chain += 1;
                        break;
                    case 3:

//                        locator = ".//*[@id='Login']";
//                        element = driver.findElement(By.xpath(locator));
//                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
                    case 4:

                        locator = ".//*[@id='AccountLogin']";
                        element = driver.findElement(By.xpath(locator));
                        text = stage.Key("Student_Username", stageArray);
                        element.sendKeys(text);

                        retry = 0;
                        chain += 1;
                        break;
                    case 5:

                        locator = ".//*[@id='Password']";
                        element = driver.findElement(By.xpath(locator));
                        text = stage.Key("Student_Authorization", stageArray);
                        element.sendKeys(text);

                        retry = 0;
                        chain += 1;
                        break;
                    case 6:

                        locator = "html/body/div[2]/form/div/div[4]/input";
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
                    case 7:

                        locator = ".//*[@id='portal-switcher']/a";
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
                    case 8:

                        locator = ".//*[@id='portal-switcher']/div/ul/li[1]/a[2]";
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
                    case 9:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cMainNavBar_MainTabNav1_tStudyL']";
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
                    case 10:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cMainNavSub_LoginView1_StudyNav1_CourseCatalog']";
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
                    case 11:

                        locator = ".//*[@id='aspnetForm']/center/div/div[2]/table/tbody/tr[1]/td/div/div/div/div[3]/table/tbody/tr/td/div[2]/div/div/div[1]/div[2]/div[1]/div/a";
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
                    case 12:

                        locator = "(//a[@class='button'][contains(.,'Begin Course')])[1]";
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;

                    case 13:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseViewer_TakeCourseLink5']";
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                    case 14:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseViewer_CourseNavigation_CourseTitle']";
                        element = driver.findElement(By.xpath(locator));

                        retry = 0;
                        chain += 1;
                        break;
                    case 15:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseViewer_CourseNavigation_NextImage']";
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
                    case 16:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseViewer_CourseNavigation_CourseTitle']";
                        element = driver.findElement(By.xpath(locator));

                        retry = 0;
                        chain += 1;
                        break;
                    case 17:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseViewer_CourseNavigation_NextImage']";
                        element = driver.findElement(By.xpath(locator));

                        retry = 0;
                        chain += 1;
                        break;
                    case 18:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseViewer_CourseNavigation_CourseTitle']";
                        element = driver.findElement(By.xpath(locator));

                        retry = 0;
                        chain += 1;
                        break;
                    case 19:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseViewer_CourseNavigation_NextImage']";
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
                    case 20:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseViewer_CourseNavigation_CourseTitle']";
                        element = driver.findElement(By.xpath(locator));

                        retry = 0;
                        chain += 1;
                        break;
                    case 21:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseViewer_CourseNavigation_NextImage']";
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
                    case 22:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseViewer_CourseNavigation_CourseTitle']";
                        element = driver.findElement(By.xpath(locator));

                        retry = 0;
                        chain += 1;
                        break;
                    case 23:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseViewer_CourseNavigation_NextImage']";
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
                    case 24:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseViewer_CourseNavigation_CourseTitle']";
                        element = driver.findElement(By.xpath(locator));

                        retry = 0;
                        chain += 1;
                        break;
                    case 25:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseViewer_CourseNavigation_NextImage']";
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
//                    case 15:
//
//                        // Hover over actions
//                        locator = "//input[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SignupForm_CreateUserWizard1_CreateUserStepContainer_CountryCode_Input']";
//                        element = driver.findElement(By.xpath(locator));
//                        builder = new Actions(driver);
//                        builder.moveToElement(element).build().perform();
//
//                        retry = 0;
//                        chain += 1;
//                        break;
//                    case 16:
//
//                        locator = "//input[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SignupForm_CreateUserWizard1_CreateUserStepContainer_CountryCode_Input'][@value='United States']";
//                        element = driver.findElement(By.xpath(locator));
//                        element.click();
//
//                        retry = 0;
//                        chain += 1;
//                        break;
//                    case 17:
//
//                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SignupForm_CreateUserWizard1_CreateUserStepContainer_TimeZones_TimeZones_Input']";
//                        element = driver.findElement(By.xpath(locator));
//                        element.click();
//
//                        retry = 0;
//                        chain += 1;
//                        break;
//                    case 18:
//
//                        locator = "//input[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SignupForm_CreateUserWizard1_CreateUserStepContainer_TimeZones_TimeZones_Input'][@value='(UTC-08:00) Pacific Time (US & Canada)']";
//                        element = driver.findElement(By.xpath(locator));
//                        element.click();
//
//                        retry = 0;
//                        chain += 1;
//                        break;
//                    case 19:
//
//                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SignupForm_CreateUserWizard1_CreateUserStepContainer_ddlOrigin_Input']";
//                        element = driver.findElement(By.xpath(locator));
//                        element.click();
//
//                        retry = 0;
//                        chain += 1;
//                        break;
//                    case 20:
//
//                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SignupForm_CreateUserWizard1_CreateUserStepContainer_ddlOrigin_DropDown']/div/ul/li[contains(.,'Friend')]";
//                        element = driver.findElement(By.xpath(locator));
//                        element.click();
//
//                        retry = 0;
//                        chain += 1;
//                        break;
//                    case 21:
//
//                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SignupForm_CreateUserWizard1_CreateUserStepContainer_TagInput1_tTags']";
//                        element = driver.findElement(By.xpath(locator));
//                        text = LzTestCourseTest.settings.Key("Tags About You", LzTestCourseTest.settingsArray);
//                        element.sendKeys(text);
//
//                        retry = 0;
//                        chain += 1;
//                        break;
//                    case 22:
//
//                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SignupForm_CreateUserWizard1_CreateUserStepContainer_AgreeCheckbox']";
//                        element = driver.findElement(By.xpath(locator));
//                        element.click();
//
//                        retry = 0;
//                        chain += 1;
//                        break;
//                    case 23:
//
//                        locator = ".//img[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SignupForm_CreateUserWizard1_CreateUserStepContainer_CaptchaNewUser_CaptchaImage']";
//                        exists = Integer.getInteger(LocateElement.XpathCount(driver, locator, LzTestCourseTest.stagePath, 5));
//
//                        if(exists == 1)
//                        {
//                            System.out.println("enter the captcha code and click create account please");
//
//                            while(exists == 1)
//                            {
//                                Thread.sleep(1000);
//                            }
//                        }
//                    case 24:
////                    default:
//                        output = "success"; // This is the last step in the chain, so that means it was successful
//                        chain = 0; // This is the last step in the chain, so set the count to zero, and exit the error-catching loop
//                        break;
                }
            } catch (UnhandledAlertException uae) {
                retry += 1;
                try {
                    Alert alert = driver.switchTo().alert();
                    text = alert.getText();
                    alert.dismiss();
                    System.out.println("Dismiss alert: " + text);
                } catch (NoAlertPresentException nape) {
                    System.out.println("No alert is active");
                }
            } catch (NoSuchElementException nsem) {
                retry += 1;
                if (retry > maxRetry) {
                    nsem.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
                text = driver.getTitle();
                if (text.toLowerCase().equals("certificate error: navigation blocked")) {

                    locator = "//a[@id='continueLink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.Edge(driver);
                    }

                    locator = "//a[@id='overridelink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.IE11(driver);
                    }
                }

                boolean firefoxBlock = true;
                while (firefoxBlock) {
                    text = driver.getTitle();
                    if (text.trim().toLowerCase().equals("insecure connection")) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    } else {
                        break;
                    }
                }

            } catch (NoSuchWindowException nswe) {
                retry += 1;
                if (retry > maxRetry) {
                    System.out.println("If this error is happening in Internet Explorer, check the Internet Properties: Security Zones. They should have the same \"Protected Mode\" check-boxes.");
                    System.out.println("If using IE11, make sure that the FEATURE_BFCACHE registry setting exists.");
                    nswe.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            } catch (UnreachableBrowserException ube) {
                retry += 1;
                if (retry > maxRetry) {
                    System.out.println("Make sure that the HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\DWORD(MaxUserPort, 65534) registry setting exists.");
                    ube.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            } catch (WebDriverException wde) {
                retry += 1;
                if (retry > maxRetry) {
                    wde.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
                text = driver.getTitle();
                if (text.toLowerCase().equals("certificate error: navigation blocked")) {
                    locator = "//a[@id='continueLink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.Edge(driver);
                    }

                    locator = "//a[@id='overridelink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.IE11(driver);
                    }
                }

                boolean firefoxBlock = true;
                while (firefoxBlock) {
                    text = driver.getTitle();
                    if (text.trim().toLowerCase().equals("insecure connection")) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    } else {
                        break;
                    }
                }
            } catch (Exception e) {
                retry += 1;
                if (retry > maxRetry) {
                    e.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        }
        return output;
    }
}
