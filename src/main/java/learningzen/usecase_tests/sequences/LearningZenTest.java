package learningzen.usecase_tests.sequences;

import learningzen.usecase_tests.Enums.LearningZenTestStatus;
import org.openqa.selenium.WebDriver;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Yi on 6/30/2016.
 */
public class LearningZenTest {
    protected WebDriver driver;

    public LearningZenTest(WebDriver driver)
    {
        this.driver = driver;
    }


    private long timeElapsed = 0;
    public long getElapsed() {
        return timeElapsed * 1000;
    }

    private LearningZenTestStatus status;
    public LearningZenTestStatus getStatus() {
        return status;
    }

    public void setStatus(LearningZenTestStatus status) {
        this.status = status;
    }

    public final void RunTest()
    {
        long startTime = System.currentTimeMillis();
        Test();
        long stopTime = System.currentTimeMillis();
        timeElapsed = stopTime - startTime;
    }

    //Always override this method, this is the actual test body
    protected void Test()
    {
        throw new org.apache.commons.lang.NotImplementedException("LearningZen test not implemented");
    }

    //Always override this method, do whatever
    public void HandleError(Exception ex)
    {
        throw new org.apache.commons.lang.NotImplementedException("LearningZen test error handling method not implemented");
    }

}
