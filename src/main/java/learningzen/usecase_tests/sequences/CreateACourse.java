package learningzen.usecase_tests.sequences;

/**
 * Created by gordonz on 6/22/2016.
 */
import java.util.ArrayList;
import java.util.List;

import common.*;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.Select;

public class CreateACourse {

    public static String CreateACourse(WebDriver driver, String[] pathArray) {

        ArraySplitter path = new ArraySplitter();
        String projectPath = path.Key(PackageManifest.project, pathArray);

        String configFilePath = projectPath+"\\src\\main\\java\\learningzen\\usecase_tests\\Config.txt";
        ArrayFromTextFile configFile = new ArrayFromTextFile();
        String[] configArray = configFile.Read(configFilePath);
        ArraySplitter config = new ArraySplitter();

        String currentStage = config.Key("Current_Stage", configArray);

        String stageFilePath = projectPath+"\\src\\main\\java\\learningzen\\usecase_tests\\"+currentStage+"Stage.txt";
        ArrayFromTextFile stageFile = new ArrayFromTextFile();
        String[] stageArray = stageFile.Read(stageFilePath);
        ArraySplitter stage = new ArraySplitter();

        String xpathMapPath = projectPath+"\\src\\main\\java\\learningzen\\usecase_tests\\XpathMap.txt";
        ArrayFromTextFile xpathMapFile = new ArrayFromTextFile();
        String[] mapArray = xpathMapFile.Read(xpathMapPath);
        ArraySplitter xpathMap = new ArraySplitter();

        String method = "CreateACourse";
        String output = null;
        int retry = 0;
        int maxRetry = 20;
        WebElement element;
        List<WebElement> elements;
        Select option = null;
        ArrayList<String> optionList = new ArrayList<String>();
        String text = null;
        Actions builder = null;
        int exists = 0;

        String locator = null;

        int chain = 1;
        while (chain > 0) {
            try {
                switch (chain) {
                    case 1:

                        locator = stage.Key("LearningZen_Url", stageArray);
                        driver.get(locator);

                        retry = 0;
                        chain += 1;
                        break;
                    case 2:

                        int windowX = Integer.parseInt(config.Key("Driver_Window_Width", configArray));
                        int windowY = Integer.parseInt(config.Key("Driver_Window_Height", configArray));
                        driver.manage().window().setSize(new Dimension(windowX, windowY));

                        retry = 0;
                        chain += 1;
                        break;
                    case 3:

//                        locator = ".//*[@id='Login']";
//                        element = driver.findElement(By.xpath(locator));
//                        element.click();
//                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 4:

                        locator = xpathMap.Key("Username_Xpath", mapArray);
                        element = driver.findElement(By.xpath(locator));

                        text = stage.Key("Portal_Author_UserName", stageArray);

                        element.sendKeys(text);
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 5:

                        locator = ".//*[@id='Password']";
                        element = driver.findElement(By.xpath(locator));

                        text = stage.Key("Portal_Author_Password", stageArray);

                        element.sendKeys(text);
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 6:

                        locator = "html/body/div[2]/form/div/div[4]/input";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 7:

                        locator = ".//*[@id='portal-switcher']/a";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 8:

                        locator = ".//*[@id='portal-switcher']/div/ul/li[1]/a[2]";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 9:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cMainNavBar_MainTabNav1_tEducateL']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 10:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_HyperLink4']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 11:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseTypeLZ']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 12:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_ChoseCourseType']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 13:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseTitle']";
                        element = driver.findElement(By.xpath(locator));

                        text = xpathMap.Key("Course Title", mapArray);

                        element.sendKeys(text);
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 14:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CategorySelector1_Category']";
                        element = driver.findElement(By.xpath(locator));

                        option = new Select(element);
                        option.selectByVisibleText("Human Resources");

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 15:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_TagInput1_tTags']";
                        element = driver.findElement(By.xpath(locator));

                        text = xpathMap.Key("Tags About You", mapArray);

                        element.sendKeys(text);
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 16:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseContentType_2']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 17:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_ShowContent']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 18:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_PreReqCoursesTitleView']";
                        element = driver.findElement(By.xpath(locator));

                        option = new Select(element);
                        option.selectByVisibleText("Big video course");

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 19:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SubNav_SubNavRepeater_ctl02_SubNavItem']/span";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 20:

                        locator = ".//*[@id='Div1']/div[2]/div[2]/div/div[1]";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 21:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SubNav_SubNavRepeater_ctl04_SubNavItem']/span";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 22:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_CourseDescriptionTop']/div/ul/li[9]/a/span";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 23:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SubNav_SubNavRepeater_ctl06_SubNavItem']/span";
                        element = driver.findElement(By.xpath(locator));

                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 24:

                        locator = ".//*[@id='Div1']/div[2]/div[2]/div[1]/div[1]";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 25:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SubNav_SubNavRepeater_ctl08_SubNavItem']/span";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 26:

                        locator = ".//*[@id='Div1']/div[2]/div[2]/div[1]/div/div/div[1]";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 27:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SubNav_SubNavRepeater_ctl10_SubNavItem']/span";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 28:

                        locator = ".//*[@id='Div1']/div[2]/div[2]/div[1]/div[1]";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 29:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_SubNav_SubNavRepeater_ctl12_SubNavItem']/span";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 30:

                        locator = ".//*[@id='Div1']/div[2]/div[2]/div[1]/div[2]/div[2]";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 31:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_TabContents_TabLinkText']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 32:

                        locator = ".//*[@id='Div1']/div[3]";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 33:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_TabIntro_TabLinkText']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 34:

                        locator = ".//*[@id='Div1']/div[2]/div/div[1]/div[1]";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 35:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_TabChapters_TabLink']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 36:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_ChapterTitle']";
                        element = driver.findElement(By.xpath(locator));

                        text = xpathMap.Key("Chapter Title", mapArray);

                        element.sendKeys(text);
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 37:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_TabReview_TabLinkText']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 38:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_ChapterEditorTitle']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 39:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_TabExam_TabLinkText']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 40:

                        locator = ".//*[@id='ctl00_ctl00_ctl00_c1_cinner_pagecontent_ExamBuilder_panel1']/div/a";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

//                        System.out.println(driver.getPageSource());

                        retry = 0;
                        chain += 1;
                        break;
                    case 41:

                        locator = xpathMap.Key("Question_Create_Iframe_Name", mapArray);
                        driver.switchTo().frame(locator);
//                        System.out.println(driver.getPageSource());

                        retry = 0;
                        chain += 1;
                        break;
                    case 42:

                        locator = ".//*[@id='ctl00_pagecontent_RequiredAlways_0']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 43:

                        locator = ".//*[@id='ctl00_pagecontent_ShowOnExam']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 44:

                        locator = ".//*[@id='ctl00_pagecontent_QuestionType']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 45:

                        locator = ".//*[@id='ctl00_pagecontent_QuestionType']/option[3]";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 46:

                        locator = ".//*[@id='ctl00_pagecontent_SaveQuestion']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 47:

                        locator = ".//*[@id='ctl00_pagecontent_RequiredAlways_0']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 48:

                        locator = ".//*[@id='ctl00_pagecontent_SaveQuestion']";
                        element = driver.findElement(By.xpath(locator));

                        element.click();
                        Thread.sleep(1000);

                        retry = 0;
                        chain += 1;
                        break;
                    case 49:
//                    default:
                        output = "success"; // This is the last step in the chain, so that means it was successful
                        chain = 0; // This is the last step in the chain, so set the count to zero, and exit the error-catching loop
                        break;
                }
            } catch (UnhandledAlertException uae) {
                retry += 1;
                try {
                    Alert alert = driver.switchTo().alert();
                    text = alert.getText();
                    alert.dismiss();
                    System.out.println("Dismiss alert: " + text);
                } catch (NoAlertPresentException nape) {
                    System.out.println("No alert is active");
                }
            } catch (NoSuchElementException nsem) {
                retry += 1;
                if (retry > maxRetry) {
                    nsem.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
                text = driver.getTitle();
                if (text.toLowerCase().equals("certificate error: navigation blocked")) {

                    locator = "//a[@id='continueLink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.Edge(driver);
                    }

                    locator = "//a[@id='overridelink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.IE11(driver);
                    }
                }

                boolean firefoxBlock = true;
                while (firefoxBlock) {
                    text = driver.getTitle();
                    if (text.trim().toLowerCase().equals("insecure connection")) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    } else {
                        break;
                    }
                }

            } catch (NoSuchWindowException nswe) {
                retry += 1;
                if (retry > maxRetry) {
                    System.out.println("If this error is happening in Internet Explorer, check the Internet Properties: Security Zones. They should have the same \"Protected Mode\" check-boxes.");
                    System.out.println("If using IE11, make sure that the FEATURE_BFCACHE registry setting exists.");
                    nswe.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            } catch (UnreachableBrowserException ube) {
                retry += 1;
                if (retry > maxRetry) {
                    System.out.println("Make sure that the HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\DWORD(MaxUserPort, 65534) registry setting exists.");
                    ube.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            } catch (WebDriverException wde) {
                retry += 1;
                if (retry > maxRetry) {
                    wde.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
                text = driver.getTitle();
                if (text.toLowerCase().equals("certificate error: navigation blocked")) {
                    locator = "//a[@id='continueLink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.Edge(driver);
                    }

                    locator = "//a[@id='overridelink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.IE11(driver);
                    }
                }

                boolean firefoxBlock = true;
                while (firefoxBlock) {
                    text = driver.getTitle();
                    if (text.trim().toLowerCase().equals("insecure connection")) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    } else {
                        break;
                    }
                }
            } catch (Exception e) {
                retry += 1;
                if (retry > maxRetry) {
                    e.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        }
        return output;
    }
}
