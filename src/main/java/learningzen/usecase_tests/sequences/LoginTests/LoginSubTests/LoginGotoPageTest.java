package learningzen.usecase_tests.sequences.LoginTests.LoginSubTests;

import learningzen.usecase_tests.sequences.LoginTests.LoginTests;
import org.openqa.selenium.WebDriver;

public class LoginGotoPageTest extends LoginTest {
    public LoginGotoPageTest(WebDriver driver) {
        super(driver);
    }

    @Override
    protected void Test() {
        String locator = LoginTests.stageConfig.getProperty("LearningZen_Url");
        driver.get(locator);
    }


}
