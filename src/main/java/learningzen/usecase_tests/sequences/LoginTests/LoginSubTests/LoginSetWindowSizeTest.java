package learningzen.usecase_tests.sequences.LoginTests.LoginSubTests;

import learningzen.usecase_tests.sequences.LoginTests.LoginTests;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

public class LoginSetWindowSizeTest extends LoginTest {
    public LoginSetWindowSizeTest(WebDriver driver) {
        super(driver);
    }

    @Override
    protected void Test() {
        int windowX = Integer.parseInt(LoginTests.rootConfig.getProperty("Driver_Window_Width"));
        int windowY = Integer.parseInt(LoginTests.rootConfig.getProperty("Driver_Window_Height"));
        driver.manage().window().setSize(new Dimension(windowX, windowY));
    }


}
