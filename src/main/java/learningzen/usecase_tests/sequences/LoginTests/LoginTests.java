package learningzen.usecase_tests.sequences.LoginTests;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import common.*;
import learningzen.usecase_tests.ConfigurationManager;
import learningzen.usecase_tests.sequences.LearningZenTest;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by miker on 5/27/2016.
 */



public class LoginTests{

    public static Properties rootConfig;
    public static Properties stageConfig;

    public static Properties xpathConfig;

    public static String As(WebDriver driver, String userKey, String[] pathArray) {
        rootConfig = ConfigurationManager.GetConfig("config.properties");
        stageConfig = ConfigurationManager.GetConfig("Stage.properties");

        String xPathFileKey = ConfigurationManager.GetConfig("TestXpathFilePath.properties").getProperty("LoginTest");
        xpathConfig = ConfigurationManager.GetConfig(xPathFileKey);
        String currentStage = rootConfig.getProperty("Current_Stage");

        String locator = "";
        String method = "Login.As "+userKey;
        String output = null;
        int retry = 0;
        int maxRetry = 20;
        WebElement element;
        List<WebElement> elements;
        Select option = null;
        ArrayList<String> optionList = new ArrayList<String>();
        String text = null;

        int chain = 1;
        while (chain > 0) {
            try {
                switch(chain) {
                    case 1:


                        break;
                    case 2:


                        retry = 0;
                        chain += 1;
                        break;
                    case 3:

                        locator = xpathConfig.getProperty("Username_Field_Xpath");
                        element = driver.findElement(By.xpath(locator));
                        text = stageConfig.getProperty(userKey + "_Username");
                        element.sendKeys(text);

                        retry = 0;
                        chain += 1;
                        break;
                    case 4:

                        locator = xpathConfig.getProperty("Password_Field_Xpath");
                        element = driver.findElement(By.xpath(locator));
                        text = stageConfig.getProperty(userKey+"_Authorization");
                        System.out.println(text);
                        element.sendKeys(text);

                        retry = 0;
                        chain += 1;
                        break;
                    case 5:

                        locator = xpathConfig.getProperty("Login_Button_Xpath");
                        element = driver.findElement(By.xpath(locator));
                        element.click();
                        ElementWait.Gone(element);

                        retry = 0;
                        chain += 1;
                        break;
                    case 6:

                        locator = xpathConfig.getProperty("Portal_Switcher_Link_Xpath");
                        element = driver.findElement(By.xpath(locator));
                        element.click();

                        retry = 0;
                        chain += 1;
                        break;
                    case 7:

                        locator = "//li/a[normalize-space(.)='Acme Amusement']";
//                        locator = xpathMap.Key("Logout_Link_Xpath", mapArray);
                        element = driver.findElement(By.xpath(locator));
                        element.click();
                        ElementWait.Gone(element);

                        retry = 0;
                        chain += 1;
                        break;
                    case 8:

                        locator = xpathConfig.getProperty("Logout_Link_Xpath");
                        element = driver.findElement(By.xpath(locator));
                        element.click();
                        ElementWait.Gone(element);

                        retry = 0;
                        chain += 1;
                        break;
                    case 9:

                        locator = xpathConfig.getProperty("Logout_Message_Xpath");
                        element = driver.findElement(By.xpath(locator));

                        retry = 0;
                        chain += 1;
                        break;
                    case 10:
//                    default:
                        output = "success"; // This is the last step in the chain, so that means it was successful
                        chain = 0; // This is the last step in the chain, so set the count to zero, and exit the error-catching loop
                        break;
                }
            } catch (UnhandledAlertException uae) {
                retry += 1;
                try {
                    Alert alert = driver.switchTo().alert();
                    text = alert.getText();
                    alert.dismiss();
                    System.out.println("Dismiss alert: " + text);
                } catch (NoAlertPresentException nape) {
                    System.out.println("No alert is active");
                }
            } catch (NoSuchElementException nsem) {
                retry += 1;
                if (retry > maxRetry) {
                    nsem.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
                text = driver.getTitle();
                if (text.toLowerCase().equals("certificate error: navigation blocked")) {

                    locator = "//a[@id='continueLink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.Edge(driver);
                    }

                    locator = "//a[@id='overridelink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.IE11(driver);
                    }
                }

                boolean firefoxBlock = true;
                while (firefoxBlock) {
                    text = driver.getTitle();
                    if (text.trim().toLowerCase().equals("insecure connection")) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    } else {
                        break;
                    }
                }

            } catch (NoSuchWindowException nswe) {
                retry += 1;
                if (retry > maxRetry) {
                    System.out.println("If this error is happening in Internet Explorer, check the Internet Properties: Security Zones. They should have the same \"Protected Mode\" check-boxes.");
                    System.out.println("If using IE11, make sure that the FEATURE_BFCACHE registry setting exists.");
                    nswe.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            } catch (UnreachableBrowserException ube) {
                retry += 1;
                if (retry > maxRetry) {
                    System.out.println("Make sure that C:\\Windows\\System32\\drivers\\etc\\hosts has localhost configured for 127.0.0.1");
                    System.out.println("Make sure that the HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\DWORD(MaxUserPort, 65534) registry setting exists.");
                    ube.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            } catch (WebDriverException wde) {
                retry += 1;
                if (retry > maxRetry) {
                    wde.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
                text = driver.getTitle();
                if (text.toLowerCase().equals("certificate error: navigation blocked")) {
                    locator = "//a[@id='continueLink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.Edge(driver);
                    }

                    locator = "//a[@id='overridelink']";
                    elements = driver.findElements(By.xpath(locator));
                    if (elements.isEmpty()) {
                        // Do nothing
                    } else {
                        CertificateErrorBypass.IE11(driver);
                    }
                }

                boolean firefoxBlock = true;
                while (firefoxBlock) {
                    text = driver.getTitle();
                    if (text.trim().toLowerCase().equals("insecure connection")) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    } else {
                        break;
                    }
                }
            } catch (Exception e) {
                retry += 1;
                if (retry > maxRetry) {
                    e.printStackTrace();
                    output = "error";
                    chain = 0;
                    break;
                }
                System.out.println("Retry " + retry + " " + method + " chain " + chain);
                try {
                    Thread.sleep(125);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        }
        return output;
    }
}
