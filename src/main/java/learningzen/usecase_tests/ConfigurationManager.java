package learningzen.usecase_tests;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by Yi on 7/1/2016.
 */
public class ConfigurationManager {
    private static String root = System.getProperty("user.dir");
    private static String configFilePath = root + "\\src\\main\\Resources\\";

    public static String GetProjectRoot() {
        return root;
    }

    public static Properties GetConfig(String fileName) {
        Properties props = new Properties();
        try {
            InputStream input = new FileInputStream(configFilePath+fileName);
            props.load(input);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return props;
    }
}
