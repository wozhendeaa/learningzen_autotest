package learningzen.usecase_tests.Enums;

/**
 * Created by Yi on 6/30/2016.
 */
public enum LearningZenTestStatus {
    Error, Running, Passed
}
